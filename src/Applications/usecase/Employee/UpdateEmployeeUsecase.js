import { UpdateEmployee } from '@/Domain/Employee';

export class UpdateEmployeeUsecase {
  constructor(employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  async execute(usecasePayload) {
    const { id, ...userData } = new UpdateEmployee(usecasePayload);
    return this.employeeRepository.update(id, userData);
  }
}
