import { AuthHandler } from './handler';

export function authRoutes(container, router, asyncHandler) {
  const authHandler = new AuthHandler(container);

  router.post(
    '/login',
    asyncHandler((...args) => authHandler.postAuthHandler(...args))
  );
  router.post(
    '/register',
    asyncHandler((...args) => authHandler.postNewAuthHandler(...args))
  );

  return ['/auth', router];
}
