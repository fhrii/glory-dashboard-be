import { LoginUsecase, RegisterUsecase } from '@/Applications/usecase/Auth';

export class AuthHandler {
  constructor(container) {
    this.container = container;
  }

  async postAuthHandler(req, res) {
    const credentials = await this.container
      .get(LoginUsecase.name)
      .execute(req.body);

    res.status(201).json({ credentials });
  }

  async postNewAuthHandler(req, res) {
    const credentials = await this.container
      .get(RegisterUsecase.name)
      .execute(req.body);

    res.status(201).json({ credentials });
  }
}
