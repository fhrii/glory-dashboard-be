export const responseTransformer = (_, res, next) => {
  const resSend = res.send;

  res.send = (...args) => {
    const newArgs = [...args];
    const response = JSON.parse(newArgs[0]);
    const isResponseString = typeof response === 'string';

    newArgs[0] = isResponseString
      ? JSON.stringify({
          statusCode: res.statusCode,
          message: response,
        })
      : JSON.stringify({
          statusCode: res.statusCode,
          data: response,
        });

    resSend.apply(res, newArgs);
  };

  next();
};
