import JoiDate from '@joi/date';
import JoiDefault from 'joi';

import { CreateEmployeePresenceSchema } from '@/Domain/EmployeePresence';

const Joi = JoiDefault.extend(JoiDate);

const CreatePresenceSchema = Joi.object({
  date: Joi.date().format('YYYY-MM-DD').required().messages({
    'date.base': 'DATE_DATE',
    'date.format': 'DATE_FORMAT',
    'any.required': 'DATE_REQUIRED',
  }),
  presenceTime: Joi.string().valid('MORNING', 'NOUN').required().messages({
    'string.base': 'PRESENCE_TIME_STRING',
    'any.only': 'PRESENCE_TIME_VALID',
    'any.required': 'PRESENCE_TIME_REQUIRED',
  }),
  employeesPresence: Joi.array().items(CreateEmployeePresenceSchema).messages({
    'array.base': 'EMPLOYEES_PRESENCE_ARRAY',
  }),
}).options({ allowUnknown: true });

export class CreatePresence {
  constructor(payload) {
    this.verifyPayload(payload);

    this.date = new Date(payload.date);
    this.presenceTime = payload.presenceTime;
    this.employeesPresence = payload.employeesPresence;
  }

  verifyPayload(payload) {
    const createPresence = CreatePresenceSchema.validate(payload);

    if (createPresence.error) {
      throw new Error(
        `CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.${createPresence.error.message}`
      );
    }
  }
}
