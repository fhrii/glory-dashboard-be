import Joi from 'joi';

import { BlockSchema } from '@/Domain/Block';
import { ConditionSchema } from '@/Domain/Condition';
import { SeedSchema } from '@/Domain/Seed/entities';

const HarvestSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  seed: SeedSchema,
  block: BlockSchema,
  condition: ConditionSchema,
  harvest: Joi.number().integer().positive().required().messages({
    'number.base': 'HARVEST_NUMBER',
    'number.integer': 'HARVEST_INTEGER',
    'number.positive': 'HARVEST_POSITIVE',
    'any.required': 'HARVEST_REQUIRED',
  }),
  date: Joi.date().required().messages({
    'date.base': 'DATE_DATE',
    'any.required': 'DATE_REQUIRED',
  }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class Harvest {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.seed = payload.seed;
    this.block = payload.block;
    this.condition = payload.condition;
    this.harvest = payload.harvest;
    this.date = payload.date;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const harvest = HarvestSchema.validate(payload);

    if (harvest.error) {
      throw new Error(
        `HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.${harvest.error.message}`
      );
    }
  }
}
