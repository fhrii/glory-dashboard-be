import { AppConfig } from '@/Infrastructures/config';
import { container } from '@/Infrastructures/di';
import { createServer } from '@/Infrastructures/http';
import { logger } from '@/Infrastructures/libs';

const appConfig = container.get(AppConfig.name);
const prisma = container.get('prisma');

async function main() {
  await prisma.$connect();
  const server = createServer(container);

  server.listen(appConfig.getPort(), () => {
    logger.info(`Server starting on port ${appConfig.getPort()}`);
  });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (error) => {
    logger.error(error);
    await prisma.$disconnect();
  });
