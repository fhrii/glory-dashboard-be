export class EmployeeRepository {
  async create(_employee) {
    throw new Error('EMPLOYEE_REPOSITORY.CREATE.METHOD_NOT_IMPLEMENTED');
  }

  async findOneById(_id) {
    throw new Error(
      'EMPLOYEE_REPOSITORY.FIND_ONE_BY_ID.METHOD_NOT_IMPLEMENTED'
    );
  }

  async findMany(_condition, _options) {
    throw new Error('EMPLOYEE_REPOSITORY.FIND_MANY.METHOD_NOT_IMPLEMENTED');
  }

  async findPagination(_condition, _options) {
    throw new Error(
      'EMPLOYEE_REPOSITORY.FIND_PAGINATION w.METHOD_NOT_IMPLEMENTED'
    );
  }

  async update(_id, _employee) {
    throw new Error('EMPLOYEE_REPOSITORY.UPDATE.METHOD_NOT_IMPLEMENTED');
  }

  async delete(_id) {
    throw new Error('EMPLOYEE_REPOSITORY.DELETE.METHOD_NOT_IMPLEMENTED');
  }

  async validateActiveIds(_ids) {
    throw new Error(
      'EMPLOYEE_REPOSITORY.VALIDATE_ACTIVE_IDS.METHOD_NOT_IMPLEMENTED'
    );
  }
}
