export class UserRepository {
  async create(_user) {
    throw new Error('USER_REPOSITORY.CREATE.METHOD_NOT_IMPLEMENTED');
  }

  async findOneByUsername(_username) {
    throw new Error('USER_REPOSITORY.FIND_BY_USERNAME.METHOD_NOT_IMPLEMENTED');
  }

  async getPasswordByUsername(_username) {
    throw new Error(
      'USER_REPOSITORY.GET_PASSWORD_BY_USERNAME.METHOD_NOT_IMPLEMENTED'
    );
  }
}
