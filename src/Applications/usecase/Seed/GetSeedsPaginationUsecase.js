import Joi from 'joi';

const GetSeedsPaginationUsecaseSchema = Joi.object({
  name: Joi.string().allow('').messages({
    'string.base': 'NAME_STRING',
  }),
  status: Joi.number().valid(1, 0).messages({
    'number.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
  }),
  skip: Joi.number().integer().min(0).messages({
    'number.base': 'SKIP_NUMBER',
    'number.integer': 'SKIP_INTEGER',
    'number.min': 'SKIP_MIN',
  }),
  take: Joi.number().integer().positive().messages({
    'number.base': 'TAKE_NUMBER',
    'number.integer': 'TAKE_INTEGER',
    'number.positive': 'TAKE_POSITIVE',
  }),
}).options({ allowUnknown: true });

export class GetSeedsPaginationUsecase {
  constructor(seedRepository) {
    this.seedRepository = seedRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { name, status, skip, take } = usecasePayload;
    let translatedStatus;

    if (status === 1) translatedStatus = 'ACTIVE';
    else if (status === 0) translatedStatus = 'NOT_ACTIVE';

    return this.seedRepository.findPagination(
      { name, status: translatedStatus },
      { skip, take }
    );
  }

  verifyPayload(payload) {
    const getSeedsPaginationUsecase =
      GetSeedsPaginationUsecaseSchema.validate(payload);

    if (getSeedsPaginationUsecase.error) {
      throw new Error(
        `GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getSeedsPaginationUsecase.error.message}`
      );
    }
  }
}
