import { protectedRoute } from '@/Infrastructures/middlewares';

import { PresenceHandler } from './handler';

export function presenceRoutes(container, router, passport, asyncHandler) {
  const presenceHandler = new PresenceHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.getPresencesHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.postPresenceHandler(...args))
  );
  router.get(
    '/statistic',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.getPresenceStatistic(...args))
  );
  router.get(
    '/statisticExcel',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) =>
      presenceHandler.getPresenceStatisticForExcel(...args)
    )
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.getPresenceHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.putPresenceHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.patchPresenceHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => presenceHandler.deletePresenceHandler(...args))
  );

  return ['/presences', router];
}
