export class ClientError extends Error {
  constructor(statusCode, message) {
    super(message);

    this.name = 'ClientError';
    this.statusCode = statusCode;

    if (this.constructor.name === 'ClientError')
      throw new Error('CLIENT_ERROR.ABSTRACT_CLASS_CANNOT_BE_INSTANTIATED');
  }
}
