export class BlockRepository {
  async create(_block) {
    throw new Error('BLOCK_REPOSITORY.CREATE.METHOD_NOT_IMPLEMENTED');
  }

  async findOneById(_id) {
    throw new Error('BLOCK_REPOSITORY.FIND_ONE_BY_ID.METHOD_NOT_IMPLEMENTED');
  }

  async findMany(_condition, _options) {
    throw new Error('BLOCK_REPOSITORY.FIND_MANY.METHOD_NOT_IMPLEMENTED');
  }

  async findPagination(_condition, _options) {
    throw new Error('BLOCK_REPOSITORY.FIND_PAGINATION.METHOD_NOT_IMPLEMENTED');
  }

  async update(_id, _block) {
    throw new Error('BLOCK_REPOSITORY.UPDATE.METHOD_NOT_IMPLEMENTED');
  }

  async delete(_id) {
    throw new Error('BLOCK_REPOSITORY.DELETE.METHOD_NOT_IMPLEMENTED');
  }
}
