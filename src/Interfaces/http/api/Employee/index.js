import { protectedRoute } from '@/Infrastructures/middlewares';

import { EmployeeHandler } from './handler';

export function employeeRoutes(container, router, passport, asyncHandler) {
  const employeeHandler = new EmployeeHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => employeeHandler.getEmployeesHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => employeeHandler.postEmployeeHandler(...args))
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => employeeHandler.getEmployeeHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => employeeHandler.putEmployeeHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => employeeHandler.patchEmployeeHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => employeeHandler.deleteEmployeeHandler(...args))
  );

  return ['/employees', router];
}
