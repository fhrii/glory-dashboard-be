import { Prisma } from '@prisma/client';

import { NotFoundError } from '@/common/exceptions';
import { Harvest, HarvestStatistic } from '@/Domain/Harvest';
import { Seed } from '@/Domain/Seed';
import {
  dayjs,
  parseDayJsToDateWithoutTime,
  parseStringToDateWithoutTime,
} from '@/utils';

export class HarvestRepositoryImpl {
  constructor(prisma, seedRepository, blockRepository, conditionRepository) {
    this.prisma = prisma;
    this.seedRepository = seedRepository;
    this.blockRepository = blockRepository;
    this.conditionRepository = conditionRepository;
  }

  async create({ harvest, date, seedId, blockId, conditionId }) {
    try {
      const harvestEntity = await this.prisma.harvest.create({
        data: {
          harvest,
          date,
          seed: { connect: { id: seedId } },
          block: { connect: { id: blockId } },
          condition: { connect: { id: conditionId } },
        },
        include: { seed: true, block: true, condition: true },
      });

      return this.harvestEntityToHarvest(harvestEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        const errorMessage = error.message;

        if (errorMessage.includes("'Seed' record(s)"))
          throw new NotFoundError(`Seed with id '${seedId}' was not found`);
        if (errorMessage.includes("'Block' record(s)"))
          throw new NotFoundError(`Block with id '${blockId}' was not found`);
        if (errorMessage.includes("'Condition' record(s)"))
          throw new NotFoundError(
            `Condition with id '${conditionId}' was not found`
          );

        throw error;
      }

      throw error;
    }
  }

  async findOneById(id) {
    const harvestEntity = await this.prisma.harvest.findUnique({
      where: { id },
      include: { seed: true, block: true, condition: true },
    });

    if (!harvestEntity)
      throw new NotFoundError(`Harvest with id '${id}' was not found`);

    return this.harvestEntityToHarvest(harvestEntity);
  }

  async findMany(
    { harvest, seed, block, condition, startDate, endDate },
    { skip, take }
  ) {
    const harvestEntities = await this.prisma.harvest.findMany({
      where: {
        harvest,
        seed: { name: { contains: seed } },
        block: { name: { contains: block } },
        condition: { name: { contains: condition } },
        date: {
          gte: startDate && parseStringToDateWithoutTime(startDate),
          lte: endDate && parseStringToDateWithoutTime(endDate),
        },
      },
      skip,
      take,
      orderBy: {
        date: 'desc',
      },
      include: { seed: true, block: true, condition: true },
    });

    return this.harvestEntitiesToHarvests(harvestEntities);
  }

  async findPagination(
    { harvest, seed, block, condition, startDate, endDate },
    { skip = 0, take }
  ) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const totalHarvests = await this.prisma.harvest.count({
      where: {
        harvest,
        seed: { name: { contains: seed } },
        block: { name: { contains: block } },
        condition: { name: { contains: condition } },
        date: {
          gte: startDate && parseStringToDateWithoutTime(startDate),
          lte: endDate && parseStringToDateWithoutTime(endDate),
        },
      },
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalHarvests / take);

    return { currentPage, totalPages };
  }

  async update(id, { harvest, date, seedId, blockId, conditionId }) {
    try {
      const harvestEntity = await this.prisma.harvest.update({
        where: { id },
        data: {
          harvest,
          date,
          seed: seedId ? { connect: { id: seedId } } : undefined,
          block: blockId ? { connect: { id: blockId } } : undefined,
          condition: conditionId ? { connect: { id: conditionId } } : undefined,
        },
        include: { seed: true, block: true, condition: true },
      });

      return this.harvestEntityToHarvest(harvestEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        const errorMessage = error.message.toLowerCase();

        if (errorMessage.includes("'Seed' record(s)"))
          throw new NotFoundError(`Seed with id '${seedId}' was not found`);
        if (errorMessage.includes("'Block' record(s)"))
          throw new NotFoundError(`Block with id '${blockId}' was not found`);
        if (errorMessage.includes("'Block' record(s)"))
          throw new NotFoundError(
            `Condition with id '${conditionId}' was not found`
          );

        throw new NotFoundError(`Harvest with id '${id}' was not found`);
      }

      throw error;
    }
  }

  async delete(id) {
    try {
      await this.prisma.harvest.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Harvest with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  async statistic({ startDate, endDate }) {
    let harvestDateGte;
    let harvestDateLte;

    if (startDate) harvestDateGte = parseStringToDateWithoutTime(startDate);
    if (endDate) harvestDateLte = parseStringToDateWithoutTime(endDate);
    if (!startDate && !endDate) {
      const today = dayjs();
      const daySevenDaysBeforeToday = today.subtract(7, 'day');
      harvestDateGte = parseDayJsToDateWithoutTime(daySevenDaysBeforeToday);
    }

    const harvestGroupByDateAndSeedEntities = await this.prisma.harvest.groupBy(
      {
        where: { date: { gte: harvestDateGte, lte: harvestDateLte } },
        by: ['date', 'seedId'],
        orderBy: {
          date: 'desc',
        },
        _sum: {
          harvest: true,
        },
      }
    );
    const allDate = harvestGroupByDateAndSeedEntities
      .filter(
        (harvestGroupByDateAndSeedEntity, index) =>
          harvestGroupByDateAndSeedEntities.findIndex(
            (aHarvestGroupByDateAndSeedEntity) =>
              aHarvestGroupByDateAndSeedEntity.date.getTime() ===
              harvestGroupByDateAndSeedEntity.date.getTime()
          ) === index
      )
      .map((group) => group.date);
    const seedIds = harvestGroupByDateAndSeedEntities
      .filter(
        (harvestGroupByDateAndSeedEntity, index) =>
          harvestGroupByDateAndSeedEntities.findIndex(
            (aHarvestGroupByDateAndSeedEntity) =>
              aHarvestGroupByDateAndSeedEntity.seedId ===
              harvestGroupByDateAndSeedEntity.seedId
          ) === index
      )
      .map((group) => group.seedId);
    const seedEntities = await this.prisma.seed.findMany({
      where: { id: { in: seedIds } },
    });
    const seeds = seedEntities.map(
      (seedEntity) =>
        new Seed({
          id: seedEntity.id,
          name: seedEntity.name,
          status: seedEntity.status,
          createdAt: seedEntity.createdAt,
          updatedAt: seedEntity.updatedAt,
        })
    );
    return allDate.map((date) => {
      const filteredHarvestGroupByDateAndSeedEntities =
        harvestGroupByDateAndSeedEntities.filter(
          (harvestGroupByDateAndSeedEntity) =>
            harvestGroupByDateAndSeedEntity.date.getTime() === date.getTime()
        );
      const seedsStatistic = filteredHarvestGroupByDateAndSeedEntities.map(
        (harvestGroupByDateAndSeedEntity) => ({
          seed: seeds.find(
            (seed) => seed.id === harvestGroupByDateAndSeedEntity.seedId
          ),
          harvest: harvestGroupByDateAndSeedEntity._sum.harvest,
        })
      );

      return new HarvestStatistic({ date, seeds: seedsStatistic });
    });
  }

  harvestEntityToHarvest({
    id,
    seed: seedEntity,
    block: blockEntity,
    condition: conditionEntity,
    harvest,
    date,
    createdAt,
    updatedAt,
  }) {
    const seed = this.seedRepository.seedEntityToSeed(seedEntity);
    const block = this.blockRepository.blockEntityToBlock(blockEntity);
    const condition =
      this.conditionRepository.conditionEntityToCondition(conditionEntity);

    return new Harvest({
      id,
      seed,
      block,
      condition,
      harvest,
      date,
      createdAt,
      updatedAt,
    });
  }

  harvestEntitiesToHarvests(harvestEntities) {
    return harvestEntities.map((harvestEntity) =>
      this.harvestEntityToHarvest(harvestEntity)
    );
  }
}
