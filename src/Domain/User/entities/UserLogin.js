import Joi from 'joi';

const UserLoginSchema = Joi.object({
  username: Joi.string().alphanum().required().messages({
    'string.base': 'USERNAME_STRING',
    'string.alphanum': 'USERNAME_ALPHANUM',
    'any.required': 'USERNAME_REQUIRED',
  }),
  password: Joi.string().required().messages({
    'string.base': 'PASSWORD_STRING',
    'any.required': 'PASSWORD_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class UserLogin {
  constructor(payload) {
    this.verifyPayload(payload);

    this.username = payload.username;
    this.password = payload.password;
  }

  verifyPayload(payload) {
    const userLogin = UserLoginSchema.validate(payload);

    if (userLogin.error) {
      throw new Error(
        `USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.${userLogin.error.message}`
      );
    }
  }
}
