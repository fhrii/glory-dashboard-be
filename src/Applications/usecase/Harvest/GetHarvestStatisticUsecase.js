import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const GetHarvestStatisticUsecaseSchema = Joi.object({
  startDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'START_DATE_DATE',
    'date.format': 'START_DATE_FORMAT',
  }),
  endDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'END_DATE_DATE',
    'date.format': 'END_DATE_FORMAT',
  }),
}).options({ allowUnknown: true });

export class GetHarvestStatisticUsecase {
  constructor(harvestRepository) {
    this.harvestRepository = harvestRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    return this.harvestRepository.statistic(usecasePayload);
  }

  verifyPayload(payload) {
    const getHarvestStatisticUsecase =
      GetHarvestStatisticUsecaseSchema.validate(payload);

    if (getHarvestStatisticUsecase.error) {
      throw new Error(
        `GET_HARVEST_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getHarvestStatisticUsecase.error.message}`
      );
    }
  }
}
