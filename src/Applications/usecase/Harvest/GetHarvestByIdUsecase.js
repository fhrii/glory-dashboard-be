import Joi from 'joi';

const GetHarvestUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetHarvestByIdUsecase {
  constructor(harvestRepository) {
    this.harvestRepository = harvestRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.harvestRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getHarvestUsecase = GetHarvestUsecaseSchema.validate(payload);

    if (getHarvestUsecase.error) {
      throw new Error(
        `GET_HARVEST_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getHarvestUsecase.error.message}`
      );
    }
  }
}
