import {
  CreateEmployeeUsecase,
  DeleteEmployeeUsecase,
  GetEmployeeByIdUsecase,
  GetEmployeesPaginationUsecase,
  GetEmployeesUsecase,
  UpdateEmployeeUsecase,
} from '@/Applications/usecase/Employee';
import { ClientError } from '@/common/exceptions';
import { EmployeePresentation } from '@/Domain/Employee';
import { parseIntOrNotNaN } from '@/utils';

export class EmployeeHandler {
  constructor(container) {
    this.container = container;
  }

  async getEmployeeHandler(req, res) {
    const employee = await this.container
      .get(GetEmployeeByIdUsecase.name)
      .execute(req.params);
    const employeePresentation = this.employeeToEmployeePresentation(employee);

    res.status(200).json({ employee: employeePresentation });
  }

  async getEmployeesHandler(req, res) {
    const { name, status, gender, skip, take } = req.query;
    const data = {
      name: name?.toLowerCase(),
      gender: gender?.toUpperCase(),
      status: parseIntOrNotNaN(status),
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [employees, employeesPagination] = await Promise.all([
      this.container.get(GetEmployeesUsecase.name).execute(data),
      this.container.get(GetEmployeesPaginationUsecase.name).execute(data),
    ]);
    const employeePresentations =
      this.employeesToEmployeePresentations(employees);

    res
      .status(200)
      .json({ employees: employeePresentations, ...employeesPagination });
  }

  async postEmployeeHandler(req, res) {
    const employee = await this.container
      .get(CreateEmployeeUsecase.name)
      .execute(req.body);
    const employeePresentation = this.employeeToEmployeePresentation(employee);

    res.status(201).json({ employee: employeePresentation });
  }

  async putEmployeeHandler(req, res) {
    const employee = await this.container
      .get(UpdateEmployeeUsecase.name)
      .execute({ ...req.params, ...req.body });
    const employeePresentation = this.employeeToEmployeePresentation(employee);

    res.status(200).json({ employee: employeePresentation });
  }

  async patchEmployeeHandler(req, res) {
    return this.putEmployeeHandler(req, res);
  }

  async deleteEmployeeHandler(req, res) {
    try {
      await this.container.get(DeleteEmployeeUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the employee');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no employee was deleted');
        return;
      }

      throw error;
    }
  }

  employeeToEmployeePresentation({ id, name, status, gender, age }) {
    return new EmployeePresentation({ id, name, status, gender, age });
  }

  employeesToEmployeePresentations(employees) {
    return employees.map((employee) =>
      this.employeeToEmployeePresentation(employee)
    );
  }
}
