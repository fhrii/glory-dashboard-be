import morgan from 'morgan';

import { logger } from '../libs';

logger.stream = {
  write: (message) =>
    logger.info(message.slice(0, Math.max(0, message.lastIndexOf('\n')))),
};

export const httpLogger = morgan(
  ':method :url :status :response-time ms :res[content-length]',
  {
    stream: logger.stream,
  }
);
