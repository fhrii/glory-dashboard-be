import { ClientError, DomainErrorTranslator } from '@/common/exceptions';

import { logger } from '../libs';

export const errorHandler = (error, _, res, __) => {
  const translatedError = DomainErrorTranslator.translate(error);

  if (translatedError instanceof ClientError) {
    res
      .status(translatedError.statusCode)
      .json({ message: translatedError.message });
    return;
  }

  logger.error(translatedError);

  res.status(500).json({
    message: 'Internal Server Error',
    error: {
      name: translatedError.name,
      message: translatedError.message,
      stack: translatedError.stack,
    },
  });
};
