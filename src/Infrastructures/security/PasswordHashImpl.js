import { BadRequestError } from '@/common/exceptions';

export class PasswordHashImpl {
  constructor(appConfig, bcrypt) {
    this.appConfig = appConfig;
    this.bcrypt = bcrypt;
  }

  async hash(password) {
    return this.bcrypt.hash(password, this.appConfig.getSaltRounds());
  }

  async comparePassword(password, hashedPassword) {
    const isMatch = await this.bcrypt.compare(password, hashedPassword);

    if (!isMatch) throw new BadRequestError('Username or password is invalid');
  }
}
