import {
  CreatePresenceUsecase,
  DeletePresenceUsecase,
  GetPresenceByIdUsecase,
  GetPresencesPaginationUsecase,
  GetPresenceStatisticForExcelUsecase,
  GetPresenceStatisticUsecase,
  GetPresencesUsecase,
  UpsertPresenceUsecase,
} from '@/Applications/usecase/Presence';
import { ClientError } from '@/common/exceptions';
import { EmployeeOnEmployeePresencePresentation } from '@/Domain/Employee';
import { EmployeePresencePresentation } from '@/Domain/EmployeePresence';
import { PresencePresentation } from '@/Domain/Presence';
import { parseIntOrNotNaN } from '@/utils';

export class PresenceHandler {
  constructor(container) {
    this.container = container;
  }

  async getPresenceHandler(req, res) {
    const presence = await this.container
      .get(GetPresenceByIdUsecase.name)
      .execute(req.params);
    const presencePresentation = this.presenceToPresencePresentation(presence);

    res.status(200).json({ presence: presencePresentation });
  }

  async getPresencesHandler(req, res) {
    const { startDate, endDate, skip, take } = req.query;
    const data = {
      startDate,
      endDate,
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [presences, presencesPagination] = await Promise.all([
      this.container.get(GetPresencesUsecase.name).execute(data),
      this.container.get(GetPresencesPaginationUsecase.name).execute(data),
    ]);
    const presencePresentations =
      this.presencesToPresencePresentations(presences);

    res
      .status(200)
      .json({ presences: presencePresentations, ...presencesPagination });
  }

  async getPresenceStatistic(req, res) {
    const statistic = await this.container
      .get(GetPresenceStatisticUsecase.name)
      .execute(req.query);

    res.status(200).json({ statistic });
  }

  async getPresenceStatisticForExcel(req, res) {
    const statisticStream = await this.container
      .get(GetPresenceStatisticForExcelUsecase.name)
      .execute(req.query);

    res.set('Content-disposition', `attachment; filename=${Date.now()}.xlsx`);
    res.set(
      'Content-Type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );

    statisticStream.pipe(res);
  }

  async postPresenceHandler(req, res) {
    const presence = await this.container
      .get(CreatePresenceUsecase.name)
      .execute(req.body);
    const presencePresentation = this.presenceToPresencePresentation(presence);

    res.status(201).json({ presence: presencePresentation });
  }

  async putPresenceHandler(req, res) {
    const presence = await this.container
      .get(UpsertPresenceUsecase.name)
      .execute({ ...req.params, ...req.body });
    const presencePresentation = this.presenceToPresencePresentation(presence);

    res.status(200).json({ presence: presencePresentation });
  }

  async patchPresenceHandler(req, res) {
    return this.putPresenceHandler(req, res);
  }

  async deletePresenceHandler(req, res) {
    try {
      await this.container.get(DeletePresenceUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the presence');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no presence was deleted');
        return;
      }

      throw error;
    }
  }

  presenceToPresencePresentation({ id, date, employeesPresence }) {
    const employeePresencePresentation = employeesPresence.map(
      ({ id: employeePresenceId, employee, presenceTime, presenceType }) =>
        new EmployeePresencePresentation({
          id: employeePresenceId,
          employee: new EmployeeOnEmployeePresencePresentation({
            id: employee.id,
            name: employee.name,
            status: employee.status,
          }),
          presenceTime,
          presenceType,
        })
    );

    return new PresencePresentation({
      id,
      date,
      employeesPresence: employeePresencePresentation,
    });
  }

  presencesToPresencePresentations(presences) {
    return presences.map((presence) =>
      this.presenceToPresencePresentation(presence)
    );
  }
}
