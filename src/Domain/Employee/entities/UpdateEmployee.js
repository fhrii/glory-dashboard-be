import Joi from 'joi';

const UpdateEmployeeSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  name: Joi.string().messages({
    'string.base': 'NAME_STRING',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
  }),
  gender: Joi.string().valid('MAN', 'WOMAN').messages({
    'string.base': 'GENDER_STRING',
    'any.only': 'GENDER_VALID',
  }),
  age: Joi.number().integer().positive().messages({
    'number.base': 'AGE_NUMBER',
    'number.integer': 'AGE_INTEGER',
    'number.positive': 'AGE_POSITIVE',
  }),
}).options({ allowUnknown: true });

export class UpdateEmployee {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.name = payload.name;
    this.status = payload.status;
    this.gender = payload.gender;
    this.age = payload.age;
  }

  verifyPayload(payload) {
    const createEmployee = UpdateEmployeeSchema.validate(payload);

    if (createEmployee.error) {
      throw new Error(
        `CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.${createEmployee.error.message}`
      );
    }
  }
}
