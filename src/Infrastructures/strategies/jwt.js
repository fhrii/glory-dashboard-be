import { ExtractJwt, Strategy } from 'passport-jwt';

import { AuthenticationError, ClientError } from '@/common/exceptions';
import { UserRepository } from '@/Domain/User';

import { AppConfig } from '../config';
import { container } from '../di';

export const jwtStrategy = new Strategy(
  {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: container.get(AppConfig.name).getAccessTokenSecret(),
  },
  (payload, done) => {
    try {
      const userRepository = container.get(UserRepository.name);
      const user = userRepository.findOneByUsername(payload.username);

      return done(null, user);
    } catch (error) {
      if (error instanceof ClientError) {
        throw new AuthenticationError('Invalid user');
      }

      throw error;
    }
  }
);
