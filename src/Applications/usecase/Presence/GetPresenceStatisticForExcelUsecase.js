import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const GetPresenceStatisticUsecaseSchema = Joi.object({
  startDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'START_DATE_DATE',
    'date.format': 'START_DATE_FORMAT',
  }),
  endDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'END_DATE_DATE',
    'date.format': 'END_DATE_FORMAT',
  }),
}).options({ allowUnknown: true });

export class GetPresenceStatisticForExcelUsecase {
  constructor(presenceRepository) {
    this.plantRepository = presenceRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    return this.plantRepository.employeeStatisticForExcel(usecasePayload);
  }

  verifyPayload(payload) {
    const getPresenceStatisticForExcelUsecase =
      GetPresenceStatisticUsecaseSchema.validate(payload);

    if (getPresenceStatisticForExcelUsecase.error) {
      throw new Error(
        `GET_PRESENCE_STATISTIC_FOR_EXCEL_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getPresenceStatisticForExcelUsecase.error.message}`
      );
    }
  }
}
