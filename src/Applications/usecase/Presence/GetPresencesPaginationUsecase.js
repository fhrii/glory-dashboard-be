import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const GetPresencesPaginationUsecaseSchema = Joi.object({
  startDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'START_DATE_DATE',
    'date.format': 'START_DATE_FORMAT',
  }),
  endDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'END_DATE_DATE',
    'date.format': 'END_DATE_FORMAT',
  }),
  skip: Joi.number().integer().min(0).messages({
    'number.base': 'SKIP_NUMBER',
    'number.integer': 'SKIP_INTEGER',
    'number.min': 'SKIP_MIN',
  }),
  take: Joi.number().integer().positive().messages({
    'number.base': 'TAKE_NUMBER',
    'number.integer': 'TAKE_INTEGER',
    'number.positive': 'TAKE_POSITIVE',
  }),
}).options({ allowUnknown: true });

export class GetPresencesPaginationUsecase {
  constructor(presenceRepository) {
    this.presenceRepository = presenceRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { startDate, endDate, skip, take } = usecasePayload;

    return this.presenceRepository.findPagination(
      { startDate, endDate },
      { skip, take }
    );
  }

  verifyPayload(payload) {
    const getPresencesPaginationUsecase =
      GetPresencesPaginationUsecaseSchema.validate(payload);

    if (getPresencesPaginationUsecase.error) {
      throw new Error(
        `GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getPresencesPaginationUsecase.error.message}`
      );
    }
  }
}
