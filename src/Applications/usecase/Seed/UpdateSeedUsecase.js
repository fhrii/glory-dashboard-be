import { UpdateSeed } from '@/Domain/Seed';

export class UpdateSeedUsecase {
  constructor(seedRepository) {
    this.seedRepository = seedRepository;
  }

  async execute(usecasePayload) {
    const { id, ...seedData } = new UpdateSeed(usecasePayload);
    return this.seedRepository.update(id, seedData);
  }
}
