import { Prisma } from '@prisma/client';

import {
  BadRequestError,
  ClientError,
  NotFoundError,
} from '@/common/exceptions';
import { Employee } from '@/Domain/Employee';
import { EmployeePresence } from '@/Domain/EmployeePresence';
import { Presence, PresenceStatistic } from '@/Domain/Presence';
import {
  dayjs,
  parseDayJsToDateWithoutTime,
  parseStringToDateWithoutTime,
  presenceTypeToIndonesian,
} from '@/utils';

export class PresenceRepositoryImpl {
  constructor(prisma, employeeRepository, employeePresenceRepository, excel) {
    this.prisma = prisma;
    this.employeeRepository = employeeRepository;
    this.employeePresenceRepository = employeePresenceRepository;
    this.excel = excel;
  }

  async create({ date, presenceTime, employeesPresence = [] }) {
    try {
      const employeesPresenceId = employeesPresence.map(
        (employeePresence) => employeePresence.employeeId
      );

      await this.employeeRepository.validateActiveIds(employeesPresenceId);

      const activeEmployees = await this.employeeRepository.findMany({
        status: 'ACTIVE',
      });

      const createEmployeesPresence = activeEmployees.map((employee) => {
        const employeeOnEmployeePresenceIndex = employeesPresence.findIndex(
          (employeePresence) => employeePresence.employeeId === employee.id
        );
        const isEmployeePresenceOnEmployeeExist =
          employeeOnEmployeePresenceIndex !== -1;

        return {
          employee: { connect: { id: employee.id } },
          presenceTime,
          presenceType: isEmployeePresenceOnEmployeeExist
            ? employeesPresence[employeeOnEmployeePresenceIndex].presenceType
            : 'ABSENT',
        };
      });

      const presenceEntity = await this.prisma.presence.upsert({
        where: {
          date,
        },
        update: {
          employeesPresence: {
            create: createEmployeesPresence,
          },
        },
        create: {
          date,
          employeesPresence: {
            create: createEmployeesPresence,
          },
        },
        include: { employeesPresence: { include: { employee: true } } },
      });

      return this.presenceEntityToPresence(presenceEntity);
    } catch (error) {
      if (error instanceof ClientError && error instanceof NotFoundError)
        throw new BadRequestError('Some employeeId was not found');

      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2002' &&
        error.message.includes(
          'employee_presences_presenceId_employeeId_presence_time_key'
        )
      )
        throw new BadRequestError(
          `Presence on ${date} and presenceTime '${presenceTime}' is already exist`
        );

      throw error;
    }
  }

  async findOneById(id) {
    const presenceEntity = await this.prisma.presence.findUnique({
      where: { id },
      include: { employeesPresence: { include: { employee: true } } },
    });

    if (!presenceEntity)
      throw new NotFoundError(`Presence with id '${id}' was not found`);

    return this.presenceEntityToPresence(presenceEntity);
  }

  async findMany({ startDate, endDate }, { skip, take }) {
    const presenceEntities = await this.prisma.presence.findMany({
      where: {
        date: {
          gte: startDate && parseStringToDateWithoutTime(startDate),
          lte: endDate && parseStringToDateWithoutTime(endDate),
        },
      },
      skip,
      take,
      orderBy: {
        date: 'desc',
      },
      include: { employeesPresence: { include: { employee: true } } },
    });

    return this.presenceEntitiesToPresences(presenceEntities);
  }

  async findPagination({ startDate, endDate }, { skip = 0, take }) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const totalPresences = await this.prisma.presence.count({
      where: {
        date: {
          gte: startDate && parseStringToDateWithoutTime(startDate),
          lte: endDate && parseStringToDateWithoutTime(endDate),
        },
      },
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalPresences / take);

    return { currentPage, totalPages };
  }

  async upsert(id, { presenceTime, employeesPresence }) {
    await this.employeePresenceRepository.upsertManyPresenceTypeByPresenceIdAndPresenceTimeAndEmployeeId(
      id,
      presenceTime,
      employeesPresence
    );
    return this.findOneById(id);
  }

  async delete(id) {
    try {
      await this.prisma.presence.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Presence with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  async statistic({ startDate, endDate }) {
    let presenceDateGte;
    let presenceDateLte;

    if (startDate) presenceDateGte = parseStringToDateWithoutTime(startDate);
    if (endDate) presenceDateLte = parseStringToDateWithoutTime(endDate);
    if (!startDate && !endDate) {
      const today = dayjs();
      const dayThirtyDaysBeforeToday = today.subtract(30, 'day');
      presenceDateGte = parseDayJsToDateWithoutTime(dayThirtyDaysBeforeToday);
    }

    const presenceEntities = await this.prisma.presence.findMany({
      where: { date: { gte: presenceDateGte, lte: presenceDateLte } },
      orderBy: { date: 'desc' },
      include: { employeesPresence: true },
    });
    const statistic = presenceEntities.map((presenceEntity) => ({
      date: presenceEntity.date,
      presences: {
        morningAbsent: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'MORNING' &&
            employeePresence.presenceType === 'ABSENT'
        ).length,
        morningPermit: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'MORNING' &&
            employeePresence.presenceType === 'PERMIT'
        ).length,
        morningSick: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'MORNING' &&
            employeePresence.presenceType === 'SICK'
        ).length,
        morningPresence: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'MORNING' &&
            employeePresence.presenceType === 'PRESENCE'
        ).length,
        nounAbsent: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'NOUN' &&
            employeePresence.presenceType === 'ABSENT'
        ).length,
        nounPermit: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'NOUN' &&
            employeePresence.presenceType === 'PERMIT'
        ).length,
        nounSick: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'NOUN' &&
            employeePresence.presenceType === 'SICK'
        ).length,
        nounPresence: presenceEntity.employeesPresence.filter(
          (employeePresence) =>
            employeePresence.presenceTime === 'NOUN' &&
            employeePresence.presenceType === 'PRESENCE'
        ).length,
      },
    }));

    return statistic.map(
      (stat) =>
        new PresenceStatistic({ date: stat.date, presences: stat.presences })
    );
  }

  async employeeStatistic({ startDate, endDate }) {
    let presenceDateGte;
    let presenceDateLte;

    if (startDate) presenceDateGte = parseStringToDateWithoutTime(startDate);
    if (endDate) presenceDateLte = parseStringToDateWithoutTime(endDate);
    if (!startDate && !endDate) {
      const today = dayjs();
      const dayThirtyDaysBeforeToday = today.subtract(29, 'day');
      presenceDateGte = parseDayJsToDateWithoutTime(dayThirtyDaysBeforeToday);
    }

    const presenceEntities = await this.prisma.presence.findMany({
      where: { date: { gte: presenceDateGte, lte: presenceDateLte } },
      orderBy: { date: 'desc' },
      include: { employeesPresence: { include: { employee: true } } },
    });
    const employeesPresence = presenceEntities.flatMap(
      (presenceEntity) => presenceEntity.employeesPresence
    );
    const unfilteredEmployees = employeesPresence.map(
      (employeePresence) => employeePresence.employee
    );
    const employees = unfilteredEmployees.filter(
      (employee, index) =>
        unfilteredEmployees.findIndex(
          (unfEmployee) => employee.id === unfEmployee.id
        ) === index
    );
    const dateLte =
      dayjs(presenceDateLte) ?? dayjs(parseDayJsToDateWithoutTime(dayjs()));
    const dateLength = dateLte.diff(dayjs(presenceDateGte), 'day');
    const rangeDays = Array.from({ length: dateLength }, (_, index) =>
      parseDayJsToDateWithoutTime(dateLte.subtract(dateLength - index, 'day'))
    );
    rangeDays.push(parseDayJsToDateWithoutTime(dateLte));

    return employees.map((employee) => {
      const statistic = rangeDays.map((date) => ({
        date,
        presenceMorning:
          presenceEntities
            .find(
              (presenceEntity) =>
                dayjs(presenceEntity.date).unix() === dayjs(date).unix()
            )
            ?.employeesPresence.find(
              (employeePresence) =>
                employeePresence.employeeId === employee.id &&
                employeePresence.presenceTime === 'MORNING'
            )?.presenceType ?? null,
        presenceNoun:
          presenceEntities
            .find(
              (presenceEntity) =>
                dayjs(presenceEntity.date).unix() === dayjs(date).unix()
            )
            ?.employeesPresence.find(
              (employeePresence) =>
                employeePresence.employeeId === employee.id &&
                employeePresence.presenceTime === 'NOUN'
            )?.presenceType ?? null,
      }));

      return {
        employeeId: employee.id,
        employeeName: employee.name,
        statistic,
      };
    });
  }

  async employeeStatisticForExcel({ startDate, endDate }) {
    const employeeStatistic = await this.employeeStatistic({
      startDate,
      endDate,
    });
    const dateRow = (
      employeeStatistic.length > 0
        ? employeeStatistic[0].statistic.map((stat) => stat.date)
        : []
    ).map((date) => ({
      value: date,
      type: Date,
      format: 'dd/mm/yyyy',
      fontWeight: 'bold',
      align: 'center',
      alignVertical: 'center',
      borderColor: '#000000',
    }));
    dateRow.unshift(null);

    let totalSalary = 0;
    const dataRow = employeeStatistic.map((empStat) => {
      const totalPresenceMorning = empStat.statistic.filter(
        (stat) => stat.presenceMorning === 'PRESENCE'
      ).length;
      const totalPresenceNoun = empStat.statistic.filter(
        (stat) => stat.presenceNoun === 'PRESENCE'
      ).length;
      const salary = totalPresenceMorning * 40_000 + totalPresenceNoun * 30_000;

      totalSalary += salary;

      return [
        empStat.employeeName,
        ...empStat.statistic.map(
          (stat) =>
            `${presenceTypeToIndonesian(
              stat.presenceMorning
            )} / ${presenceTypeToIndonesian(stat.presenceNoun)}`
        ),
        totalPresenceMorning,
        totalPresenceNoun,
        salary,
      ].map((value, index) => ({
        value,
        align: index === 0 ? 'left' : 'center',
        borderColor: '#000000',
      }));
    });

    dataRow.push([
      ...Array.from({
        length: dateRow.length + 2,
      }).fill(null),
      {
        value: totalSalary,
        align: 'center',
        borderColor: '#000000',
      },
    ]);

    const headerRow = [
      {
        value: 'Nama',
        fontWeight: 'bold',
        rowSpan: 2,
        align: 'center',
        alignVertical: 'center',
        borderColor: '#000000',
      },
      {
        value: 'Tanggal',
        fontWeight: 'bold',
        span: dateRow.length > 0 ? dateRow.length - 1 : dateRow.length,
        align: 'center',
        alignVertical: 'center',
        borderColor: '#000000',
      },
      ...Array.from({
        length: dateRow.length > 0 ? dateRow.length - 2 : dateRow.length,
      }).fill(null),
      {
        value: 'Presensi Pagi',
        fontWeight: 'bold',
        rowSpan: 2,
        align: 'center',
        alignVertical: 'center',
        borderColor: '#000000',
      },
      {
        value: 'Presensi Siang',
        fontWeight: 'bold',
        rowSpan: 2,
        align: 'center',
        alignVertical: 'center',
        borderColor: '#000000',
      },
      {
        value: 'Gaji',
        fontWeight: 'bold',
        rowSpan: 2,
        align: 'center',
        alignVertical: 'center',
        borderColor: '#000000',
      },
    ];
    const columnSize = dataRow.length > 0 ? dataRow[0].length : 0;
    const columns = Array.from({ length: columnSize }, (_, index) => ({
      width: index === 0 ? 24 : 19,
    }));

    return this.excel.write([headerRow, dateRow, ...dataRow], {
      columns,
      fontSize: 10,
    });
  }

  presenceEntityToPresence({
    id,
    date,
    employeesPresence: employeePresenceEntities,
    createdAt,
    updatedAt,
  }) {
    const employeesPresence = employeePresenceEntities.map(
      ({
        id: employeePresenceid,
        employee,
        presenceTime,
        presenceType,
        createdAt: employeePresenceCreatedAt,
        updatedAt: employeePresenceUpdatedAt,
      }) =>
        new EmployeePresence({
          id: employeePresenceid,
          employee: new Employee({
            id: employee.id,
            name: employee.name,
            status: employee.status,
            gender: employee.gender,
            age: employee.age,
            createdAt: employee.createdAt,
            updatedAt: employee.updatedAt,
          }),
          presenceTime,
          presenceType,
          createdAt: employeePresenceCreatedAt,
          updatedAt: employeePresenceUpdatedAt,
        })
    );

    return new Presence({
      id,
      date,
      employeesPresence,
      createdAt,
      updatedAt,
    });
  }

  presenceEntitiesToPresences(seedEntities) {
    return seedEntities.map((seedEntity) =>
      this.presenceEntityToPresence(seedEntity)
    );
  }
}
