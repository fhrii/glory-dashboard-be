import {
  CreatePlantUsecase,
  DeletePlantUsecase,
  GetPlantByIdUsecase,
  GetPlantsPaginationUsecase,
  GetPlantStatisticUsecase,
  GetPlantsUsecase,
  UpdatePlantUsecase,
} from '@/Applications/usecase/Plant';
import { ClientError } from '@/common/exceptions';
import { BlockPresentation } from '@/Domain/Block';
import { PlantPresentation, PlantStatisticPresentation } from '@/Domain/Plant';
import { SeedPresentation } from '@/Domain/Seed';
import { parseIntOrNotNaN } from '@/utils';

export class PlantHandler {
  constructor(container) {
    this.container = container;
  }

  async getPlantHandler(req, res) {
    const plant = await this.container
      .get(GetPlantByIdUsecase.name)
      .execute(req.params);
    const plantPresentation = this.plantToPlantPresentation(plant);

    res.status(200).json({ plant: plantPresentation });
  }

  async getPlantsHandler(req, res) {
    const { plant, seed, block, startDate, endDate, skip, take } = req.query;
    const data = {
      plant: parseIntOrNotNaN(plant),
      seed: seed?.toLowerCase(),
      block: block?.toLowerCase(),
      startDate,
      endDate,
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [plants, plantsPagination] = await Promise.all([
      this.container.get(GetPlantsUsecase.name).execute(data),
      this.container.get(GetPlantsPaginationUsecase.name).execute(data),
    ]);
    const plantPresentations = this.plantsToPlantPresentations(plants);

    res.status(200).json({ plants: plantPresentations, ...plantsPagination });
  }

  async getPlantStatistic(req, res) {
    const statistic = await this.container
      .get(GetPlantStatisticUsecase.name)
      .execute(req.query);
    const statisticPresentation = statistic.map(
      ({ date, seeds }) =>
        new PlantStatisticPresentation({
          date,
          seeds: seeds.map(({ seed, plant }) => ({
            seed: new SeedPresentation({
              id: seed.id,
              name: seed.name,
              status: seed.status,
            }),
            plant,
          })),
        })
    );

    res.status(200).json({ statistic: statisticPresentation });
  }

  async postPlantHandler(req, res) {
    const plant = await this.container
      .get(CreatePlantUsecase.name)
      .execute(req.body);
    const plantPresentation = this.plantToPlantPresentation(plant);

    res.status(201).json({ plant: plantPresentation });
  }

  async putPlantHandler(req, res) {
    const plant = await this.container
      .get(UpdatePlantUsecase.name)
      .execute({ ...req.params, ...req.body });
    const plantPresentation = this.plantToPlantPresentation(plant);

    res.status(200).json({ plant: plantPresentation });
  }

  async patchPlantHandler(req, res) {
    return this.putPlantHandler(req, res);
  }

  async deletePlantHandler(req, res) {
    try {
      await this.container.get(DeletePlantUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the plant');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no plant was deleted');
        return;
      }

      throw error;
    }
  }

  plantToPlantPresentation({ id, seed, block, plant, date }) {
    const seedPresentation = new SeedPresentation({
      id: seed.id,
      name: seed.name,
      status: seed.status,
    });
    const blockPresentation = new BlockPresentation({
      id: block.id,
      name: block.name,
      status: block.status,
    });

    return new PlantPresentation({
      id,
      seed: seedPresentation,
      block: blockPresentation,
      plant,
      date,
    });
  }

  plantsToPlantPresentations(plants) {
    return plants.map((plant) => this.plantToPlantPresentation(plant));
  }
}
