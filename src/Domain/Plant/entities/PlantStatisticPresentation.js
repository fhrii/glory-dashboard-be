import Joi from 'joi';

import { SeedPresentationSchema } from '@/Domain/Seed';

const PlantStatisticPresentationSchema = Joi.object({
  date: Joi.date().required().messages({}),
  seeds: Joi.array()
    .items(
      Joi.object({
        seed: SeedPresentationSchema,
        plant: Joi.number().integer().min(0).required().messages({
          'number.base': 'PLANT_NUMBER',
          'number.integer': 'PLANT_INTEGER',
          'number.min': 'PLANT_MIN',
          'any.required': 'PLANT_REQUIRED',
        }),
      })
    )
    .messages({
      'array.base': 'STATISTIC_ARRAY',
    }),
});

export class PlantStatisticPresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.date = payload.date;
    this.seeds = payload.seeds;
  }

  verifyPayload(payload) {
    const plantStatisticPresentation =
      PlantStatisticPresentationSchema.validate(payload);

    if (plantStatisticPresentation.error) {
      throw new Error(
        `PLANT_STATISTIC_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${plantStatisticPresentation.error.message}`
      );
    }
  }
}
