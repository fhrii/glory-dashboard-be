export class DeleteBlockUsecase {
  constructor(blockRepository) {
    this.blockRepository = blockRepository;
  }

  async execute({ id }) {
    return this.blockRepository.delete(id);
  }
}
