import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const CreateHarvestSchema = Joi.object({
  seedId: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  blockId: Joi.string().alphanum().required().messages({
    'string.base': 'BLOCK_ID_STRING',
    'string.alphanum': 'BLOCK_ID_ALPHANUM',
    'any.required': 'BLOCK_ID_REQUIRED',
  }),
  conditionId: Joi.string().alphanum().required().messages({
    'string.base': 'CONDITION_ID_STRING',
    'string.alphanum': 'CONDITION_ID_ALPHANUM',
    'any.required': 'CONDITION_ID_REQUIRED',
  }),
  harvest: Joi.number().integer().positive().required().messages({
    'number.base': 'HARVEST_NUMBER',
    'number.integer': 'HARVEST_INTEGER',
    'number.positive': 'HARVEST_POSITIVE',
    'any.required': 'HARVEST_REQUIRED',
  }),
  date: Joi.date().format('YYYY-MM-DD').required().messages({
    'date.base': 'DATE_DATE',
    'date.format': 'DATE_FORMAT',
    'any.required': 'DATE_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class CreateHarvest {
  constructor(payload) {
    this.verifyPayload(payload);

    this.seedId = payload.seedId;
    this.blockId = payload.blockId;
    this.conditionId = payload.conditionId;
    this.harvest = payload.harvest;
    this.date = new Date(payload.date);
  }

  verifyPayload(payload) {
    const createHarvest = CreateHarvestSchema.validate(payload);

    if (createHarvest.error) {
      throw new Error(
        `CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.${createHarvest.error.message}`
      );
    }
  }
}
