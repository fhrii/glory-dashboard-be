export class DeletePresenceUsecase {
  constructor(presenceRepository) {
    this.presenceRepository = presenceRepository;
  }

  async execute({ id }) {
    return this.presenceRepository.delete(id);
  }
}
