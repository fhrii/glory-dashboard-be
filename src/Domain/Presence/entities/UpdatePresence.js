import Joi from 'joi';

import { UpdateEmployeePresenceSchema } from '@/Domain/EmployeePresence';

const UpdatePresenceSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  presenceTime: Joi.string().valid('MORNING', 'NOUN').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'EMPLOYEE_ID_REQUIRED',
  }),
  employeesPresence: Joi.array()
    .items(UpdateEmployeePresenceSchema)
    .min(1)
    .required()
    .messages({
      'array.base': 'EMPLOYEES_PRESENCE_ARRAY',
      'array.min': 'EMPLOYEES_PRESENCE_MIN',
      'any.required': 'EMPLOYEES_PRESENCE_REQUIRED',
    }),
}).options({ allowUnknown: true });

export class UpdatePresence {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.presenceTime = payload.presenceTime;
    this.employeesPresence = payload.employeesPresence;
  }

  verifyPayload(payload) {
    const updatePresence = UpdatePresenceSchema.validate(payload);

    if (updatePresence.error) {
      throw new Error(
        `UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.${updatePresence.error.message}`
      );
    }
  }
}
