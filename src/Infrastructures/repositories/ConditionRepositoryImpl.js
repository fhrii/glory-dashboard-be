import { Prisma } from '@prisma/client';

import { BadRequestError, NotFoundError } from '@/common/exceptions';
import { Condition } from '@/Domain/Condition';

export class ConditionRepositoryImpl {
  constructor(prisma) {
    this.prisma = prisma;
  }

  async create({ name, status }) {
    try {
      const conditionEntity = await this.prisma.condition.create({
        data: {
          name,
          status,
        },
      });

      return this.conditionEntityToCondition(conditionEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2002'
      ) {
        throw new BadRequestError(
          `Condition with name '${name}' is already exist`
        );
      }

      throw error;
    }
  }

  async findOneById(id) {
    const conditionEntity = await this.prisma.condition.findUnique({
      where: { id },
    });

    if (!conditionEntity)
      throw new NotFoundError(`Condition with id '${id}' was not found`);

    return this.conditionEntityToCondition(conditionEntity);
  }

  async findMany({ name, status }, { skip, take }) {
    const conditionEntities = await this.prisma.condition.findMany({
      where: { name: { contains: name }, status },
      skip,
      take,
      orderBy: {
        name: 'asc',
      },
    });

    return this.conditionEntitiesToConditions(conditionEntities);
  }

  async findPagination({ name, status }, { skip = 0, take }) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const conditions = {};

    if (name) conditions.name = { contains: name };
    if (status) conditions.status = status;

    const totalConditions = await this.prisma.condition.count({
      where: Object.keys(conditions).length > 0 ? conditions : undefined,
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalConditions / take);

    return { currentPage, totalPages };
  }

  async update(id, { name, status }) {
    try {
      const conditionEntity = await this.prisma.condition.update({
        where: { id },
        data: {
          name,
          status,
        },
      });

      return this.conditionEntityToCondition(conditionEntity);
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002')
          throw new BadRequestError(
            `Condition with name '${name}' is already exist'`
          );
        if (error.code === 'P2025')
          throw new NotFoundError(`Condition with id '${id}' was not found`);
      }

      throw error;
    }
  }

  async delete(id) {
    try {
      await this.prisma.condition.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Condition with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  conditionEntityToCondition({ id, name, status, createdAt, updatedAt }) {
    return new Condition({
      id,
      name,
      status,
      createdAt,
      updatedAt,
    });
  }

  conditionEntitiesToConditions(conditionEntities) {
    return conditionEntities.map((conditionEntity) =>
      this.conditionEntityToCondition(conditionEntity)
    );
  }
}
