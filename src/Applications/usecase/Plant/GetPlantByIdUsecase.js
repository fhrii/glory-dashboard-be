import Joi from 'joi';

const GetPlantUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetPlantByIdUsecase {
  constructor(plantRepository) {
    this.plantRepository = plantRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.plantRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getPlantUsecase = GetPlantUsecaseSchema.validate(payload);

    if (getPlantUsecase.error) {
      throw new Error(
        `GET_PLANT_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getPlantUsecase.error.message}`
      );
    }
  }
}
