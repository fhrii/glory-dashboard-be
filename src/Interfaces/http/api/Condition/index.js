import { protectedRoute } from '@/Infrastructures/middlewares';

import { ConditionHandler } from './handler';

export function conditionRoutes(container, router, passport, asyncHandler) {
  const conditionHandler = new ConditionHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => conditionHandler.getConditionsHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => conditionHandler.postConditionHandler(...args))
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => conditionHandler.getConditionHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => conditionHandler.putConditionHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => conditionHandler.patchConditionHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => conditionHandler.deleteConditionHandler(...args))
  );

  return ['/conditions', router];
}
