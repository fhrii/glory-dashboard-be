export class DeleteConditionUsecase {
  constructor(conditionRepository) {
    this.conditionRepository = conditionRepository;
  }

  async execute({ id }) {
    return this.conditionRepository.delete(id);
  }
}
