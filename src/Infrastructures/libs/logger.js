import 'winston-daily-rotate-file';

import { createLogger, format, transports } from 'winston';

const loggerDailyRotateFileConfig = {
  maxFiles: '14',
  datePattern: 'YYYY-MM-DD',
  maxSize: '20m',
};

export const logger = createLogger({
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
    new transports.DailyRotateFile({
      filename: './logs/log-%DATE%.log',
      ...loggerDailyRotateFileConfig,
    }),
    new transports.DailyRotateFile({
      level: 'error',
      filename: './logs/errors/log-%DATE%.log',
      ...loggerDailyRotateFileConfig,
    }),
    new transports.Console(),
  ],
});
