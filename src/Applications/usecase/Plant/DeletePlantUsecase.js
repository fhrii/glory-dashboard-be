export class DeletePlantUsecase {
  constructor(plantRepository) {
    this.plantRepository = plantRepository;
  }

  async execute({ id }) {
    return this.plantRepository.delete(id);
  }
}
