import {
  CreateBlockUsecase,
  DeleteBlockUsecase,
  GetBlockByIdUsecase,
  GetBlocksPaginationUsecase,
  GetBlocksUsecase,
  UpdateBlockUsecase,
} from '@/Applications/usecase/Block';
import { ClientError } from '@/common/exceptions';
import { BlockPresentation } from '@/Domain/Block';
import { parseIntOrNotNaN } from '@/utils';

export class BlockHandler {
  constructor(container) {
    this.container = container;
  }

  async getBlockHandler(req, res) {
    const block = await this.container
      .get(GetBlockByIdUsecase.name)
      .execute(req.params);
    const blockPresentation = this.blockToBlockPresentation(block);

    res.status(200).json({ block: blockPresentation });
  }

  async getBlocksHandler(req, res) {
    const { name, status, skip, take } = req.query;
    const data = {
      name: name?.toLowerCase(),
      status: parseIntOrNotNaN(status),
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [blocks, blocksPagination] = await Promise.all([
      this.container.get(GetBlocksUsecase.name).execute(data),
      this.container.get(GetBlocksPaginationUsecase.name).execute(data),
    ]);
    const blockPresentations = this.blocksToBlockPresentations(blocks);

    res.status(200).json({ blocks: blockPresentations, ...blocksPagination });
  }

  async postBlockHandler(req, res) {
    const block = await this.container
      .get(CreateBlockUsecase.name)
      .execute(req.body);
    const blockPresentation = this.blockToBlockPresentation(block);

    res.status(201).json({ block: blockPresentation });
  }

  async putBlockHandler(req, res) {
    const block = await this.container
      .get(UpdateBlockUsecase.name)
      .execute({ ...req.params, ...req.body });
    const blockPresentation = this.blockToBlockPresentation(block);

    res.status(200).json({ block: blockPresentation });
  }

  async patchBlockHandler(req, res) {
    return this.putBlockHandler(req, res);
  }

  async deleteBlockHandler(req, res) {
    try {
      await this.container.get(DeleteBlockUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the block');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no block was deleted');
        return;
      }

      throw error;
    }
  }

  blockToBlockPresentation({ id, name, status }) {
    return new BlockPresentation({ id, name, status });
  }

  blocksToBlockPresentations(blocks) {
    return blocks.map((block) => this.blockToBlockPresentation(block));
  }
}
