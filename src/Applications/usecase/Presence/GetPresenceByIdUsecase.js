import Joi from 'joi';

const GetPresenceUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetPresenceByIdUsecase {
  constructor(presenceRepository) {
    this.presenceRepository = presenceRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.presenceRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getPresenceUsecase = GetPresenceUsecaseSchema.validate(payload);

    if (getPresenceUsecase.error) {
      throw new Error(
        `GET_PRESENCE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getPresenceUsecase.error.message}`
      );
    }
  }
}
