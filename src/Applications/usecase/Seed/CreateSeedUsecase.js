import { CreateSeed } from '@/Domain/Seed';

export class CreateSeedUsecase {
  constructor(seedRepository) {
    this.seedRepository = seedRepository;
  }

  async execute(usecasePayload) {
    const createSeed = new CreateSeed(usecasePayload);
    return this.seedRepository.create(createSeed);
  }
}
