export function parseDayJsToDateWithoutTime(dayjsDate) {
  return new Date(dayjsDate.format('YYYY-MM-DD'));
}
