import { CreateEmployee } from '@/Domain/Employee';

export class CreateEmployeeUsecase {
  constructor(employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  async execute(usecasePayload) {
    const createEmployee = new CreateEmployee(usecasePayload);
    return this.employeeRepository.create(createEmployee);
  }
}
