import Joi from 'joi';

const NewAuthSchema = Joi.object({
  accessToken: Joi.string().required().messages({
    'string.base': 'ACCESS_TOKEN_STRING',
    'any.required': 'ACCESS_TOKEN_REQUIRED',
  }),
});

export class NewAuth {
  constructor(payload) {
    this.verifyPayload(payload);

    this.accessToken = payload.accessToken;
  }

  verifyPayload(payload) {
    const newAuth = NewAuthSchema.validate(payload);

    if (newAuth.error) {
      throw new Error(
        `NEW_AUTH.NOT_MEET_DATA_TYPE_SPECIFICATION.${newAuth.error.message}`
      );
    }
  }
}
