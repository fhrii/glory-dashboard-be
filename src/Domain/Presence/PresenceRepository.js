export class PresenceRepository {
  async create(_presence) {
    throw new Error('PRESENCE_REPOSITORY.CREATE.METHOD_NOT_IMPLEMENTED');
  }

  async findOneById(_id) {
    throw new Error(
      'PRESENCE_REPOSITORY.FIND_ONE_BY_ID.METHOD_NOT_IMPLEMENTED'
    );
  }

  async findMany(_condition, _options) {
    throw new Error('PRESENCE_REPOSITORY.FIND_MANY.METHOD_NOT_IMPLEMENTED');
  }

  async upsert(_id, _presence) {
    throw new Error('PRESENCE_REPOSITORY.UPSERT.METHOD_NOT_IMPLEMENTED');
  }

  async delete(_id) {
    throw new Error('PRESENCE_REPOSITORY.DELETE.METHOD_NOT_IMPLEMENTED');
  }

  async statistic(_condition) {
    throw new Error('PRESENCE_REPOSITORY.STATISTIC.METHOD_NOT_IMPLEMENTED');
  }

  async employeeStatistic(_condition) {
    throw new Error(
      'PRESENCE_REPOSITORY.EMPLOYEE_STATISTIC.METHOD_NOT_IMPLEMENTED'
    );
  }

  async employeeStatisticForExcel(_condition) {
    throw new Error(
      'PRESENCE_REPOSITORY.EMPLOYEE_STATISTIC_FOR_EXCEL.METHOD_NOT_IMPLEMENTED'
    );
  }
}
