import Joi from 'joi';

const GetBlockUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetBlockByIdUsecase {
  constructor(blockRepository) {
    this.blockRepository = blockRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.blockRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getBlockUsecase = GetBlockUsecaseSchema.validate(payload);

    if (getBlockUsecase.error) {
      throw new Error(
        `GET_BLOCK_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getBlockUsecase.error.message}`
      );
    }
  }
}
