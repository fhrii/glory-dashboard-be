import dayjsDefault from 'dayjs';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';

export const dayjs = dayjsDefault;
dayjs.extend(utc);
dayjs.extend(timezone);
