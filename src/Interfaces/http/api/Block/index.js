import { protectedRoute } from '@/Infrastructures/middlewares';

import { BlockHandler } from './handler';

export function blockRoutes(container, router, passport, asyncHandler) {
  const blockHandler = new BlockHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => blockHandler.getBlocksHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => blockHandler.postBlockHandler(...args))
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => blockHandler.getBlockHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => blockHandler.putBlockHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => blockHandler.patchBlockHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => blockHandler.deleteBlockHandler(...args))
  );

  return ['/blocks', router];
}
