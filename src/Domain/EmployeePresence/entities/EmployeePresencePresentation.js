import Joi from 'joi';

import { EmployeeOnEmployeePresencePresentationSchema } from '@/Domain/Employee';

export const EmployeePresencePresentationSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  employee: EmployeeOnEmployeePresencePresentationSchema,
  presenceTime: Joi.string().valid('MORNING', 'NOUN').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
  presenceType: Joi.string()
    .valid('ABSENT', 'PERMIT', 'SICK', 'PRESENCE')
    .required()
    .messages({
      'string.base': 'PRESENCE_TYPE_STRING',
      'any.only': 'PRESENCE_TYPE_VALID',
      'any.required': 'PRESENCE_TYPE_REQUIRED',
    }),
});

export class EmployeePresencePresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.employee = payload.employee;
    this.presenceTime = payload.presenceTime;
    this.presenceType = payload.presenceType;
  }

  verifyPayload(payload) {
    const employeePresencePresentation =
      EmployeePresencePresentationSchema.validate(payload);

    if (employeePresencePresentation.error) {
      throw new Error(
        `EMPLOYEE_PRESENCE_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${employeePresencePresentation.error.message}`
      );
    }
  }
}
