import Joi from 'joi';

const CreateSeedSchema = Joi.object({
  name: Joi.string().required().messages({
    'string.base': 'NAME_STRING',
    'string.alphanum': 'NAME_ALPHANUM',
    'any.required': 'NAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class CreateSeed {
  constructor(payload) {
    this.verifyPayload(payload);

    this.name = payload.name;
    this.status = payload.status;
  }

  verifyPayload(payload) {
    const createSeed = CreateSeedSchema.validate(payload);

    if (createSeed.error) {
      throw new Error(
        `CREATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.${createSeed.error.message}`
      );
    }
  }
}
