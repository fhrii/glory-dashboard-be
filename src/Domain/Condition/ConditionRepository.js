export class ConditionRepository {
  async create(_condition) {
    throw new Error('CONDITION_REPOSITORY.CREATE.METHOD_NOT_IMPLEMENTED');
  }

  async findOneById(_id) {
    throw new Error(
      'CONDITION_REPOSITORY.FIND_ONE_BY_CONDITION.METHOD_NOT_IMPLEMENTED'
    );
  }

  async findMany(_condition, _options) {
    throw new Error('CONDITION_REPOSITORY.FIND_MANY.METHOD_NOT_IMPLEMENTED');
  }

  async findPagination(_condition, _options) {
    throw new Error(
      'CONDITION_REPOSITORY.FIND_PAGINATION.METHOD_NOT_IMPLEMENTED'
    );
  }

  async update(_id, _condition) {
    throw new Error('CONDITION_REPOSITORY.UPDATE.METHOD_NOT_IMPLEMENTED');
  }

  async delete(_id) {
    throw new Error('CONDITION_REPOSITORY.DELETE.METHOD_NOT_IMPLEMENTED');
  }
}
