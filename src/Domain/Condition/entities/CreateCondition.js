import Joi from 'joi';

const CreateConditionSchema = Joi.object({
  name: Joi.string().required().messages({
    'string.base': 'NAME_STRING',
    'any.required': 'NAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class CreateCondition {
  constructor(payload) {
    this.verifyPayload(payload);

    this.name = payload.name;
    this.status = payload.status;
  }

  verifyPayload(payload) {
    const createCondition = CreateConditionSchema.validate(payload);

    if (createCondition.error) {
      throw new Error(
        `CREATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.${createCondition.error.message}`
      );
    }
  }
}
