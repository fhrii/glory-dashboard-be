import { UpdateCondition } from '@/Domain/Condition';

export class UpdateConditionUsecase {
  constructor(conditionRepository) {
    this.conditionRepository = conditionRepository;
  }

  async execute(usecasePayload) {
    const { id, ...conditionData } = new UpdateCondition(usecasePayload);
    return this.conditionRepository.update(id, conditionData);
  }
}
