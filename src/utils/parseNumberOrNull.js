export function parseIntOrNotNaN(number) {
  const parsedNumber = parseInt(number, 10);
  const isNaN = Number.isNaN(parsedNumber);

  return isNaN ? undefined : parsedNumber;
}
