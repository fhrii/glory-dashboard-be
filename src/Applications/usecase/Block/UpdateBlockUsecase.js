import { UpdateBlock } from '@/Domain/Block';

export class UpdateBlockUsecase {
  constructor(blockRepository) {
    this.blockRepository = blockRepository;
  }

  async execute(usecasePayload) {
    const { id, ...blockData } = new UpdateBlock(usecasePayload);
    return this.blockRepository.update(id, blockData);
  }
}
