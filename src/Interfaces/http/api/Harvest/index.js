import { protectedRoute } from '@/Infrastructures/middlewares';

import { HarvestHandler } from './handler';

export function harvestRoutes(container, router, passport, asyncHandler) {
  const harvestHandler = new HarvestHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.getHarvestsHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.postHarvestHandler(...args))
  );
  router.get(
    '/statistic',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.getHarvestStatistic(...args))
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.getHarvestHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.putHarvestHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.patchHarvestHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => harvestHandler.deleteHarvestHandler(...args))
  );

  return ['/harvests', router];
}
