import { CreatePresence } from '@/Domain/Presence';

export class CreatePresenceUsecase {
  constructor(presenceRepository) {
    this.presenceRepository = presenceRepository;
  }

  async execute(usecasePayload) {
    const createPresence = new CreatePresence(usecasePayload);
    return this.presenceRepository.create(createPresence);
  }
}
