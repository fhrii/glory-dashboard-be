import { CreatePlant } from '@/Domain/Plant';

export class CreatePlantUsecase {
  constructor(plantRepository) {
    this.plantRepository = plantRepository;
  }

  async execute(usecasePayload) {
    const createPlant = new CreatePlant(usecasePayload);
    return this.plantRepository.create(createPlant);
  }
}
