import Joi from 'joi';

import { EmployeePresencePresentationSchema } from '@/Domain/EmployeePresence';

export const PresencePresentationSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  date: Joi.date().required().messages({
    'date.base': 'DATE_DATE',
    'any.required': 'DATE_REQUIRED',
  }),
  employeesPresence: Joi.array()
    .items(EmployeePresencePresentationSchema)
    .required()
    .messages({
      'array.base': 'EMPLOYEE_PRESENCE_ARRAY',
      'any.required': 'EMPLOYEE_PRESENCE_REQUIRED',
    }),
});

export class PresencePresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.date = payload.date;
    this.employeesPresence = payload.employeesPresence;
  }

  verifyPayload(payload) {
    const presencePresentation = PresencePresentationSchema.validate(payload);

    if (presencePresentation.error) {
      throw new Error(
        `PRESENCE_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${presencePresentation.error.message}`
      );
    }
  }
}
