import Joi from 'joi';

const PresenceStatisticSchema = Joi.object({
  date: Joi.date().required().messages({}),
  presences: Joi.object({
    morningAbsent: Joi.number().integer().required().messages({
      'number.base': 'MORNING_ABSENT_NUMBER',
      'number.integer': 'MORNING_ABSENT_INTEGER',
      'any.required': 'MORNING_ABSENT_REQUIRED',
    }),
    morningPermit: Joi.number().integer().required().messages({
      'number.base': 'MORNING_PERMIT_NUMBER',
      'number.integer': 'MORNING_PERMIT_INTEGER',
      'any.required': 'MORNING_PERMIT_REQUIRED',
    }),
    morningSick: Joi.number().integer().required().messages({
      'number.base': 'MORNING_SICK_NUMBER',
      'number.integer': 'MORNING_SICK_INTEGER',
      'any.required': 'MORNING_SICK_REQUIRED',
    }),
    morningPresence: Joi.number().integer().required().messages({
      'number.base': 'MORNING_PRESENCE_NUMBER',
      'number.integer': 'MORNING_PRESENCE_INTEGER',
      'any.required': 'MORNING_PRESENCE_REQUIRED',
    }),
    nounAbsent: Joi.number().integer().required().messages({
      'number.base': 'NOUN_ABSENT_NUMBER',
      'number.integer': 'NOUN_ABSENT_INTEGER',
      'any.required': 'NOUN_ABSENT_REQUIRED',
    }),
    nounPermit: Joi.number().integer().required().messages({
      'number.base': 'NOUN_PERMIT_NUMBER',
      'number.integer': 'NOUN_PERMIT_INTEGER',
      'any.required': 'NOUN_PERMIT_REQUIRED',
    }),
    nounSick: Joi.number().integer().required().messages({
      'number.base': 'NOUN_SICK_NUMBER',
      'number.integer': 'NOUN_SICK_INTEGER',
      'any.required': 'NOUN_SICK_REQUIRED',
    }),
    nounPresence: Joi.number().integer().required().messages({
      'number.base': 'NOUN_PRESENCE_NUMBER',
      'number.integer': 'NOUN_PRESENCE_INTEGER',
      'any.required': 'NOUN_PRESENCE_REQUIRED',
    }),
  }),
});

export class PresenceStatistic {
  constructor(payload) {
    this.verifyPayload(payload);

    this.date = payload.date;
    this.presences = payload.presences;
  }

  verifyPayload(payload) {
    const presenceStatistic = PresenceStatisticSchema.validate(payload);

    if (presenceStatistic.error) {
      throw new Error(
        `PRESENCE_STATISTIC.NOT_MEET_DATA_TYPE_SPECIFICATION.${presenceStatistic.error.message}`
      );
    }
  }
}
