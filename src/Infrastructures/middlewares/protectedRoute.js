import { AuthenticationError, AuthorizationError } from '@/common/exceptions';

export const protectedRoute = (req, res, next, passport) => {
  passport.authenticate('jwt', (error, user, info) => {
    if (info?.name === 'Error' && info?.message === 'No auth token')
      throw new AuthorizationError('Authorization error. Please login first');
    if (info?.name === 'TokenExpiredError')
      throw new AuthenticationError('Expired token');
    if (info?.name === 'JsonWebTokenError')
      throw new AuthenticationError('Invalid token');
    if (error || info?.name === 'Error') throw error || info;

    req.user = user;
    next();
  })(req, res, next);
};
