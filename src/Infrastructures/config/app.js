export class AppConfig {
  getEnv() {
    return process.env.NODE_ENV;
  }

  getPort() {
    return process.env.PORT;
  }

  getSaltRounds() {
    return +process.env.SALT_ROUNDS;
  }

  getAccessTokenSecret() {
    return new TextEncoder().encode(process.env.ACCESS_TOKEN_SECRET);
  }
}
