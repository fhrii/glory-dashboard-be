import {
  CreateSeedUsecase,
  DeleteSeedUsecase,
  GetSeedByIdUsecase,
  GetSeedsPaginationUsecase,
  GetSeedsUsecase,
  UpdateSeedUsecase,
} from '@/Applications/usecase/Seed';
import { ClientError } from '@/common/exceptions';
import { SeedPresentation } from '@/Domain/Seed';
import { parseIntOrNotNaN } from '@/utils';

export class SeedHandler {
  constructor(container) {
    this.container = container;
  }

  async getSeedHandler(req, res) {
    const seed = await this.container
      .get(GetSeedByIdUsecase.name)
      .execute(req.params);
    const seedPresentation = this.seedToSeedPresentation(seed);

    res.status(200).json({ seed: seedPresentation });
  }

  async getSeedsHandler(req, res) {
    const { name, status, skip, take } = req.query;
    const data = {
      name: name?.toLowerCase(),
      status: parseIntOrNotNaN(status),
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [seeds, seedsPagination] = await Promise.all([
      this.container.get(GetSeedsUsecase.name).execute(data),
      this.container.get(GetSeedsPaginationUsecase.name).execute(data),
    ]);
    const seedPresentations = this.seedsToSeedPresentations(seeds);

    res.status(200).json({ seeds: seedPresentations, ...seedsPagination });
  }

  async postSeedHandler(req, res) {
    const seed = await this.container
      .get(CreateSeedUsecase.name)
      .execute(req.body);
    const seedPresentation = this.seedToSeedPresentation(seed);

    res.status(201).json({ seed: seedPresentation });
  }

  async putSeedHandler(req, res) {
    const seed = await this.container
      .get(UpdateSeedUsecase.name)
      .execute({ ...req.params, ...req.body });
    const seedPresentation = this.seedToSeedPresentation(seed);

    res.status(200).json({ seed: seedPresentation });
  }

  async patchSeedHandler(req, res) {
    return this.putSeedHandler(req, res);
  }

  async deleteSeedHandler(req, res) {
    try {
      await this.container.get(DeleteSeedUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the seed');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no seed was deleted');
        return;
      }

      throw error;
    }
  }

  seedToSeedPresentation({ id, name, status }) {
    return new SeedPresentation({ id, name, status });
  }

  seedsToSeedPresentations(seeds) {
    return seeds.map((seed) => this.seedToSeedPresentation(seed));
  }
}
