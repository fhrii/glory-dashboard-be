import Joi from 'joi';

export const EmployeeOnEmployeePresencePresentationSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  name: Joi.string().required().messages({
    'string.base': 'USERNAME_STRING',
    'any.required': 'USERNAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
});

export class EmployeeOnEmployeePresencePresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.name = payload.name;
    this.status = payload.status;
  }

  verifyPayload(payload) {
    const employeeOnEmployeePresencePresentation =
      EmployeeOnEmployeePresencePresentationSchema.validate(payload);

    if (employeeOnEmployeePresencePresentation.error) {
      throw new Error(
        `EMPLOYEE_ON_EMPLOYEE_PRESENCE_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${employeeOnEmployeePresencePresentation.error.message}`
      );
    }
  }
}
