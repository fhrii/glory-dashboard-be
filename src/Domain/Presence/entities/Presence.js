import Joi from 'joi';

import { EmployeePresenceSchema } from '@/Domain/EmployeePresence';

export const PresenceSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  date: Joi.date().required().messages({
    'date.base': 'DATE_DATE',
    'any.required': 'DATE_REQUIRED',
  }),
  employeesPresence: Joi.array()
    .items(EmployeePresenceSchema)
    .required()
    .messages({
      'array.base': 'EMPLOYEE_PRESENCE_ARRAY',
      'any.required': 'EMPLOYEE_PRESENCE_REQUIRED',
    }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class Presence {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.date = payload.date;
    this.employeesPresence = payload.employeesPresence;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const presence = PresenceSchema.validate(payload);

    if (presence.error) {
      throw new Error(
        `PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.${presence.error.message}`
      );
    }
  }
}
