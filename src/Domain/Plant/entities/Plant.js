import Joi from 'joi';

import { BlockSchema } from '@/Domain/Block';
import { SeedSchema } from '@/Domain/Seed/entities';

const PlantSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  seed: SeedSchema,
  block: BlockSchema,
  plant: Joi.number().integer().positive().required().messages({
    'number.base': 'PLANT_NUMBER',
    'number.integer': 'PLANT_INTEGER',
    'number.positive': 'PLANT_POSITIVE',
    'any.required': 'PLANT_REQUIRED',
  }),
  date: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class Plant {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.seed = payload.seed;
    this.block = payload.block;
    this.plant = payload.plant;
    this.date = payload.date;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const plant = PlantSchema.validate(payload);

    if (plant.error) {
      throw new Error(
        `PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.${plant.error.message}`
      );
    }
  }
}
