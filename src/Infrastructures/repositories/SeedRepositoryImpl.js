import { Prisma } from '@prisma/client';

import { BadRequestError, NotFoundError } from '@/common/exceptions';
import { Seed } from '@/Domain/Seed';

export class SeedRepositoryImpl {
  constructor(prisma) {
    this.prisma = prisma;
  }

  async create({ name, status }) {
    try {
      const seedEntity = await this.prisma.seed.create({
        data: {
          name,
          status,
        },
      });

      return this.seedEntityToSeed(seedEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2002'
      ) {
        throw new BadRequestError(`Seed with name '${name}' is already exist`);
      }

      throw error;
    }
  }

  async findOneById(id) {
    const seedEntity = await this.prisma.seed.findUnique({ where: { id } });

    if (!seedEntity)
      throw new NotFoundError(`Seed with id '${id}' was not found`);

    return this.seedEntityToSeed(seedEntity);
  }

  async findMany({ name, status }, { skip, take }) {
    const conditions = {};

    if (name) conditions.name = { contains: name };
    if (status) conditions.status = status;

    const seedEntities = await this.prisma.seed.findMany({
      where: Object.keys(conditions).length > 0 ? conditions : undefined,
      skip,
      take,
      orderBy: {
        name: 'asc',
      },
    });

    return this.seedEntitiesToSeeds(seedEntities);
  }

  async findPagination({ name, status }, { skip = 0, take }) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const conditions = {};

    if (name) conditions.name = { contains: name };
    if (status) conditions.status = status;

    const totalSeeds = await this.prisma.seed.count({
      where: Object.keys(conditions).length > 0 ? conditions : undefined,
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalSeeds / take);

    return { currentPage, totalPages };
  }

  async update(id, { name, status }) {
    try {
      const seedEntity = await this.prisma.seed.update({
        where: { id },
        data: {
          name,
          status,
        },
      });

      return this.seedEntityToSeed(seedEntity);
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002')
          throw new BadRequestError(
            `Seed with name '${name}' is already exist`
          );
        if (error.code === 'P2025')
          throw new NotFoundError(`Seed with id '${id}' was not found`);
      }

      throw error;
    }
  }

  async delete(id) {
    try {
      await this.prisma.seed.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Seed with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  seedEntityToSeed({ id, name, status, createdAt, updatedAt }) {
    return new Seed({
      id,
      name,
      status,
      createdAt,
      updatedAt,
    });
  }

  seedEntitiesToSeeds(seedEntities) {
    return seedEntities.map((seedEntity) => this.seedEntityToSeed(seedEntity));
  }
}
