import Joi from 'joi';

const UpdateConditionSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  name: Joi.string().messages({
    'string.base': 'NAME_STRING',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
  }),
}).options({ allowUnknown: true });

export class UpdateCondition {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.name = payload.name;
    this.status = payload.status;
  }

  verifyPayload(payload) {
    const updateCondition = UpdateConditionSchema.validate(payload);

    if (updateCondition.error) {
      throw new Error(
        `UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.${updateCondition.error.message}`
      );
    }
  }
}
