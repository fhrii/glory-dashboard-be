import { BadRequestError, ClientError } from '@/common/exceptions';
import { NewAuth } from '@/Domain/Auth';
import { UserLogin } from '@/Domain/User';

export class LoginUsecase {
  constructor(userRepository, passwordHash, tokenManager) {
    this.userRepository = userRepository;
    this.passwordHash = passwordHash;
    this.tokenManager = tokenManager;
  }

  async execute(usecasePayload) {
    try {
      const userLogin = new UserLogin(usecasePayload);
      const hashedPassword = await this.userRepository.getPasswordByUsername(
        userLogin.username
      );

      await this.passwordHash.comparePassword(
        userLogin.password,
        hashedPassword
      );

      const user = await this.userRepository.findOneByUsername(
        userLogin.username
      );

      const accessToken = await this.tokenManager.createAccessToken({
        ...user,
      });

      return new NewAuth({ accessToken });
    } catch (error) {
      if (error instanceof ClientError) {
        throw new BadRequestError('Username or password are invalid');
      }

      throw error;
    }
  }
}
