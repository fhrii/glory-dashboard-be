import Joi from 'joi';

import { EmployeeSchema } from '@/Domain/Employee';

export const EmployeePresenceSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  employee: EmployeeSchema,
  presenceTime: Joi.string().valid('MORNING', 'NOUN').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
  presenceType: Joi.string()
    .valid('ABSENT', 'PERMIT', 'SICK', 'PRESENCE')
    .required()
    .messages({
      'string.base': 'PRESENCE_TYPE_STRING',
      'any.only': 'PRESENCE_TYPE_VALID',
      'any.required': 'PRESENCE_TYPE_REQUIRED',
    }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class EmployeePresence {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.employee = payload.employee;
    this.presenceTime = payload.presenceTime;
    this.presenceType = payload.presenceType;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const employeePresence = EmployeePresenceSchema.validate(payload);

    if (employeePresence.error) {
      throw new Error(
        `EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.${employeePresence.error.message}`
      );
    }
  }
}
