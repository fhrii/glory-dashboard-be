import { protectedRoute } from '@/Infrastructures/middlewares';

import { SeedHandler } from './handler';

export function seedRoutes(container, router, passport, asyncHandler) {
  const seedHandler = new SeedHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => seedHandler.getSeedsHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => seedHandler.postSeedHandler(...args))
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => seedHandler.getSeedHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => seedHandler.putSeedHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => seedHandler.patchSeedHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => seedHandler.deleteSeedHandler(...args))
  );

  return ['/seeds', router];
}
