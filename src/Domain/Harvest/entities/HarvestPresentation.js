import Joi from 'joi';

import { BlockPresentationSchema } from '@/Domain/Block';
import { ConditionPresentationSchema } from '@/Domain/Condition';
import { SeedPresentationSchema } from '@/Domain/Seed/entities';

const HarvestPresentationSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  seed: SeedPresentationSchema,
  block: BlockPresentationSchema,
  condition: ConditionPresentationSchema,
  harvest: Joi.number().integer().positive().required().messages({
    'number.base': 'HARVEST_NUMBER',
    'number.integer': 'HARVEST_INTEGER',
    'number.positive': 'HARVEST_POSITIVE',
    'any.required': 'HARVEST_REQUIRED',
  }),
  date: Joi.date().required().messages({
    'date.base': 'DATE_DATE',
    'any.required': 'DATE_REQUIRED',
  }),
});

export class HarvestPresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.seed = payload.seed;
    this.block = payload.block;
    this.condition = payload.condition;
    this.harvest = payload.harvest;
    this.date = payload.date;
  }

  verifyPayload(payload) {
    const harvestPresentation = HarvestPresentationSchema.validate(payload);

    if (harvestPresentation.error) {
      throw new Error(
        `HARVEST_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${harvestPresentation.error.message}`
      );
    }
  }
}
