import Joi from 'joi';

const GetConditionUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetConditionByIdUsecase {
  constructor(conditionRepository) {
    this.conditionRepository = conditionRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.conditionRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getConditionUsecase = GetConditionUsecaseSchema.validate(payload);

    if (getConditionUsecase.error) {
      throw new Error(
        `GET_CONDITION_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getConditionUsecase.error.message}`
      );
    }
  }
}
