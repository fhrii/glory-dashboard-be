import Joi from 'joi';

export const SeedPresentationSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  name: Joi.string().required().messages({
    'string.base': 'NAME_STRING',
    'any.required': 'NAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
});

export class SeedPresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.name = payload.name;
    this.status = payload.status;
  }

  verifyPayload(payload) {
    const seedPresentation = SeedPresentationSchema.validate(payload);

    if (seedPresentation.error) {
      throw new Error(
        `SEED_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${seedPresentation.error.message}`
      );
    }
  }
}
