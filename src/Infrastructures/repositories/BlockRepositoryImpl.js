import { Prisma } from '@prisma/client';

import { BadRequestError, NotFoundError } from '@/common/exceptions';
import { Block } from '@/Domain/Block';

export class BlockRepositoryImpl {
  constructor(prisma) {
    this.prisma = prisma;
  }

  async create({ name, status }) {
    try {
      const blockEntity = await this.prisma.block.create({
        data: {
          name,
          status,
        },
      });

      return this.blockEntityToBlock(blockEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2002'
      ) {
        throw new BadRequestError(`Block with name '${name}' is already exist`);
      }

      throw error;
    }
  }

  async findOneById(id) {
    const blockEntity = await this.prisma.block.findUnique({ where: { id } });

    if (!blockEntity)
      throw new NotFoundError(`Block with id '${id}' was not found`);

    return this.blockEntityToBlock(blockEntity);
  }

  async findMany({ name, status }, { skip, take }) {
    const blockEntities = await this.prisma.block.findMany({
      where: { name: { contains: name }, status },
      skip,
      take,
      orderBy: {
        name: 'asc',
      },
    });

    return this.blockEntitiesToBlocks(blockEntities);
  }

  async findPagination({ name, status }, { skip = 0, take }) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const conditions = {};

    if (name) conditions.name = { contains: name };
    if (status) conditions.status = status;

    const totalBlocks = await this.prisma.block.count({
      where: Object.keys(conditions).length > 0 ? conditions : undefined,
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalBlocks / take);

    return { currentPage, totalPages };
  }

  async update(id, { name, status }) {
    try {
      const blockEntity = await this.prisma.block.update({
        where: { id },
        data: {
          name,
          status,
        },
      });

      return this.blockEntityToBlock(blockEntity);
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002')
          throw new BadRequestError(
            `Block with name '${name}' is already exist'`
          );
        if (error.code === 'P2025')
          throw new NotFoundError(`Block with id '${id}' was not found`);
      }

      throw error;
    }
  }

  async delete(id) {
    try {
      await this.prisma.block.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Block with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  blockEntityToBlock({ id, name, status, createdAt, updatedAt }) {
    return new Block({
      id,
      name,
      status,
      createdAt,
      updatedAt,
    });
  }

  blockEntitiesToBlocks(blockEntities) {
    return blockEntities.map((blockEntity) =>
      this.blockEntityToBlock(blockEntity)
    );
  }
}
