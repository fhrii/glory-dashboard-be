import { AuthenticationError } from '@/common/exceptions';

export class TokenManagerImpl {
  constructor(appConfig, jose) {
    this.appConfig = appConfig;
    this.jose = jose;
  }

  async createAccessToken(payload) {
    return new this.jose.SignJWT(payload)
      .setProtectedHeader({ alg: 'HS256' })
      .setIssuedAt()
      .setExpirationTime('7d')
      .sign(this.appConfig.getAccessTokenSecret());
  }

  async verifyAccessToken(token) {
    try {
      await this.jose.jwtVerify(token, this.appConfig.getAccessTokenSecret());
    } catch {
      throw new AuthenticationError('Invalid access token');
    }
  }
}
