export class EmployeePresenceRepository {
  async upsertPresenceTypeByPresenceIdAndEmployeeIdAndPresenceTime(
    _presenceId,
    _employeeId,
    _presenceTime,
    _presenceType
  ) {
    throw new Error(
      'EMPLOYEE_PRESENCE_REPOSITORY.UPSERT_PRESENCE_TYPE_BY_PRESENCE_ID_AND_EMLOYEE_ID_AND_PRESENCE_TIME.METHOD_NOT_IMPLEMENTED'
    );
  }
}
