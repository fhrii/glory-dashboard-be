import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const GetHarvestsPaginationUsecaseSchema = Joi.object({
  harvest: Joi.number().integer().min(0).messages({
    'number.base': 'HARVEST_NUMBER',
    'number.integer': 'HARVEST_INTEGER',
    'number.min': 'HARVEST_MIN',
  }),
  seed: Joi.string().allow('').messages({
    'string.base': 'SEED_STRING',
  }),
  block: Joi.string().allow('').messages({
    'string.base': 'BLOCK_STRING',
  }),
  condition: Joi.string().allow('').messages({
    'string.base': 'CONDITION_STRING',
  }),
  startDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'START_DATE_DATE',
    'date.format': 'START_DATE_FORMAT',
  }),
  endDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'END_DATE_DATE',
    'date.format': 'END_DATE_FORMAT',
  }),
  skip: Joi.number().integer().min(0).messages({
    'number.base': 'SKIP_NUMBER',
    'number.integer': 'SKIP_INTEGER',
    'number.min': 'SKIP_MIN',
  }),
  take: Joi.number().integer().positive().messages({
    'number.base': 'TAKE_NUMBER',
    'number.integer': 'TAKE_INTEGER',
    'number.positive': 'TAKE_POSITIVE',
  }),
}).options({ allowUnknown: true });

export class GetHarvestsPaginationUsecase {
  constructor(harvestRepository) {
    this.harvestRepository = harvestRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { harvest, seed, block, condition, startDate, endDate, skip, take } =
      usecasePayload;
    return this.harvestRepository.findPagination(
      { harvest, seed, block, condition, startDate, endDate },
      { skip, take }
    );
  }

  verifyPayload(payload) {
    const getHarvestsPaginationUsecase =
      GetHarvestsPaginationUsecaseSchema.validate(payload);

    if (getHarvestsPaginationUsecase.error) {
      throw new Error(
        `GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getHarvestsPaginationUsecase.error.message}`
      );
    }
  }
}
