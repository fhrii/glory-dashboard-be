import { Prisma } from '@prisma/client';

import { NotFoundError } from '@/common/exceptions';
import { Employee } from '@/Domain/Employee';
import { EmployeePresence } from '@/Domain/EmployeePresence';

export class EmployeePresenceRepositoryImpl {
  constructor(prisma) {
    this.prisma = prisma;
  }

  async upsertManyPresenceTypeByPresenceIdAndPresenceTimeAndEmployeeId(
    presenceId,
    presenceTime,
    employeesPresence
  ) {
    try {
      const employeePresenceTransaction = employeesPresence.map(
        (employeePresence) =>
          this.prisma.employeePresence.upsert({
            where: {
              presenceId_employeeId_presenceTime: {
                presenceId,
                presenceTime,
                employeeId: employeePresence.employeeId,
              },
            },
            update: {
              presenceType: employeePresence.presenceType,
            },
            create: {
              presence: { connect: { id: presenceId } },
              employee: { connect: { id: employeePresence.employeeId } },
              presenceTime,
              presenceType: employeePresence.presenceType,
            },
            include: { employee: true },
          })
      );
      const employeePresenceEntities = await this.prisma.$transaction(
        employeePresenceTransaction
      );

      return this.employeePresenceEntitiesToEmployeesPresence(
        employeePresenceEntities
      );
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      )
        throw new NotFoundError(
          `Presence with id '${presenceId}' and presenceTime '${presenceTime}' or some employeeId was not found for this presence'`
        );

      throw error;
    }
  }

  employeePresenceEntityToEmployeePresence({
    id,
    employee,
    presenceTime,
    presenceType,
    createdAt,
    updatedAt,
  }) {
    return new EmployeePresence({
      id,
      employee: new Employee({
        id: employee.id,
        name: employee.name,
        status: employee.status,
        gender: employee.gender,
        age: employee.age,
        createdAt: employee.createdAt,
        updatedAt: employee.updatedAt,
      }),
      presenceTime,
      presenceType,
      createdAt,
      updatedAt,
    });
  }

  employeePresenceEntitiesToEmployeesPresence(employeePresenceEntities) {
    return employeePresenceEntities.map((employeePresence) =>
      this.employeePresenceEntityToEmployeePresence(employeePresence)
    );
  }
}
