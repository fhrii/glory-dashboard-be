import { NewAuth } from '@/Domain/Auth';
import { UserLogin } from '@/Domain/User';

export class RegisterUsecase {
  constructor(userRepository, passwordHash, tokenManager) {
    this.userRepository = userRepository;
    this.passwordHash = passwordHash;
    this.tokenManager = tokenManager;
  }

  async execute(usecasePayload) {
    const userLogin = new UserLogin(usecasePayload);
    const hashedPassword = await this.passwordHash.hash(userLogin.password);
    const user = await this.userRepository.create({
      username: userLogin.username,
      password: hashedPassword,
    });
    const accessToken = await this.tokenManager.createAccessToken({ ...user });

    return new NewAuth({ accessToken });
  }
}
