export class DeleteHarvestUsecase {
  constructor(harvestRepository) {
    this.harvestRepository = harvestRepository;
  }

  async execute({ id }) {
    return this.harvestRepository.delete(id);
  }
}
