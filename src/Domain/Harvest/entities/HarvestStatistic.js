import Joi from 'joi';

import { SeedSchema } from '@/Domain/Seed';

const HarvestStatisticSchema = Joi.object({
  date: Joi.date().required().messages({}),
  seeds: Joi.array()
    .items(
      Joi.object({
        seed: SeedSchema,
        harvest: Joi.number().integer().min(0).required().messages({
          'number.base': 'HARVEST_NUMBER',
          'number.integer': 'HARVEST_INTEGER',
          'number.min': 'HARVEST_MIN',
          'any.required': 'HARVEST_REQUIRED',
        }),
      })
    )
    .messages({
      'array.base': 'STATISTIC_ARRAY',
    }),
});

export class HarvestStatistic {
  constructor(payload) {
    this.verifyPayload(payload);

    this.date = payload.date;
    this.seeds = payload.seeds;
  }

  verifyPayload(payload) {
    const harvestStatistic = HarvestStatisticSchema.validate(payload);

    if (harvestStatistic.error) {
      throw new Error(
        `HARVEST_STATISTIC.NOT_MEET_DATA_TYPE_SPECIFICATION.${harvestStatistic.error.message}`
      );
    }
  }
}
