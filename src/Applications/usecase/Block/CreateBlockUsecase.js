import { CreateBlock } from '@/Domain/Block';

export class CreateBlockUsecase {
  constructor(blockRepository) {
    this.blockRepository = blockRepository;
  }

  async execute(usecasePayload) {
    const createBlock = new CreateBlock(usecasePayload);
    return this.blockRepository.create(createBlock);
  }
}
