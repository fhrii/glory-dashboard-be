import Joi from 'joi';

export const EmployeeSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  name: Joi.string().required().messages({
    'string.base': 'USERNAME_STRING',
    'any.required': 'USERNAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
  gender: Joi.string().valid('MAN', 'WOMAN').required().messages({
    'string.base': 'GENDER_STRING',
    'any.only': 'GENDER_VALID',
    'any.required': 'GENDER_REQUIRED',
  }),
  age: Joi.number().integer().positive().required().messages({
    'number.base': 'AGE_NUMBER',
    'number.integer': 'AGE_INTEGER',
    'number.positive': 'AGE_POSITIVE',
    'any.required': 'AGE_REQUIRED',
  }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class Employee {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.name = payload.name;
    this.status = payload.status;
    this.gender = payload.gender;
    this.age = payload.age;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const employee = EmployeeSchema.validate(payload);

    if (employee.error) {
      throw new Error(
        `EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.${employee.error.message}`
      );
    }
  }
}
