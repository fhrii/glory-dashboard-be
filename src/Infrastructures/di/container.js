import { PrismaClient } from '@prisma/client';
import {
  ContainerBuilder,
  PackageReference,
  Reference,
} from 'node-dependency-injection';

import { PasswordHash, TokenManager } from '@/Applications/security';
import { LoginUsecase, RegisterUsecase } from '@/Applications/usecase/Auth';
import {
  CreateBlockUsecase,
  DeleteBlockUsecase,
  GetBlockByIdUsecase,
  GetBlocksPaginationUsecase,
  GetBlocksUsecase,
  UpdateBlockUsecase,
} from '@/Applications/usecase/Block';
import {
  CreateConditionUsecase,
  DeleteConditionUsecase,
  GetConditionByIdUsecase,
  GetConditionsPaginationUsecase,
  GetConditionsUsecase,
  UpdateConditionUsecase,
} from '@/Applications/usecase/Condition';
import {
  CreateEmployeeUsecase,
  DeleteEmployeeUsecase,
  GetEmployeeByIdUsecase,
  GetEmployeesPaginationUsecase,
  GetEmployeesUsecase,
  UpdateEmployeeUsecase,
} from '@/Applications/usecase/Employee';
import {
  CreateHarvestUsecase,
  DeleteHarvestUsecase,
  GetHarvestByIdUsecase,
  GetHarvestsPaginationUsecase,
  GetHarvestStatisticUsecase,
  GetHarvestsUsecase,
  UpdateHarvestUsecase,
} from '@/Applications/usecase/Harvest';
import {
  CreatePlantUsecase,
  DeletePlantUsecase,
  GetPlantByIdUsecase,
  GetPlantsPaginationUsecase,
  GetPlantStatisticUsecase,
  GetPlantsUsecase,
  UpdatePlantUsecase,
} from '@/Applications/usecase/Plant';
import {
  CreatePresenceUsecase,
  DeletePresenceUsecase,
  GetPresenceByIdUsecase,
  GetPresencesPaginationUsecase,
  GetPresenceStatisticForExcelUsecase,
  GetPresenceStatisticUsecase,
  GetPresencesUsecase,
  UpsertPresenceUsecase,
} from '@/Applications/usecase/Presence';
import {
  CreateSeedUsecase,
  DeleteSeedUsecase,
  GetSeedByIdUsecase,
  GetSeedsPaginationUsecase,
  GetSeedsUsecase,
  UpdateSeedUsecase,
} from '@/Applications/usecase/Seed';
import { Excel } from '@/Applications/utilities';
import { BlockRepository } from '@/Domain/Block';
import { ConditionRepository } from '@/Domain/Condition';
import { EmployeeRepository } from '@/Domain/Employee';
import { EmployeePresenceRepository } from '@/Domain/EmployeePresence';
import { HarvestRepository } from '@/Domain/Harvest';
import { PlantRepository } from '@/Domain/Plant';
import { PresenceRepository } from '@/Domain/Presence';
import { SeedRepository } from '@/Domain/Seed';
import { UserRepository } from '@/Domain/User';

import { AppConfig } from '../config';
import {
  BlockRepositoryImpl,
  ConditionRepositoryImpl,
  EmployeePresenceRepositoryImpl,
  EmployeeRepositoryImpl,
  HarvestRepositoryImpl,
  PlantRepositoryImpl,
  PresenceRepositoryImpl,
  SeedRepositoryImpl,
  UserRepositoryImpl,
} from '../repositories';
import { PasswordHashImpl, TokenManagerImpl } from '../security';
import { ExcelImpl } from '../utilities';

const container = new ContainerBuilder();

container.register(AppConfig.name, AppConfig);
container.register('prisma', PrismaClient);

container
  .register(PasswordHash.name, PasswordHashImpl)
  .addArgument(new Reference(AppConfig.name))
  .addArgument(new PackageReference('bcrypt'));
container
  .register(TokenManager.name, TokenManagerImpl)
  .addArgument(new Reference(AppConfig.name))
  .addArgument(new PackageReference('jose'));
container
  .register(Excel.name, ExcelImpl)
  .addArgument(new PackageReference('write-excel-file/node'));

container
  .register(UserRepository.name, UserRepositoryImpl)
  .addArgument(new Reference('prisma'));
container
  .register(EmployeeRepository.name, EmployeeRepositoryImpl)
  .addArgument(new Reference('prisma'));
container
  .register(SeedRepository.name, SeedRepositoryImpl)
  .addArgument(new Reference('prisma'));
container
  .register(BlockRepository.name, BlockRepositoryImpl)
  .addArgument(new Reference('prisma'));
container
  .register(ConditionRepository.name, ConditionRepositoryImpl)
  .addArgument(new Reference('prisma'));
container
  .register(PlantRepository.name, PlantRepositoryImpl)
  .addArgument(new Reference('prisma'))
  .addArgument(new Reference(SeedRepository.name))
  .addArgument(new Reference(BlockRepository.name));
container
  .register(HarvestRepository.name, HarvestRepositoryImpl)
  .addArgument(new Reference('prisma'))
  .addArgument(new Reference(SeedRepository.name))
  .addArgument(new Reference(BlockRepository.name))
  .addArgument(new Reference(ConditionRepository.name));
container
  .register(PresenceRepository.name, PresenceRepositoryImpl)
  .addArgument(new Reference('prisma'))
  .addArgument(new Reference(EmployeeRepository.name))
  .addArgument(new Reference(EmployeePresenceRepository.name))
  .addArgument(new Reference(Excel.name));
container
  .register(EmployeePresenceRepository.name, EmployeePresenceRepositoryImpl)
  .addArgument(new Reference('prisma'));

container
  .register(LoginUsecase.name, LoginUsecase)
  .addArgument(new Reference(UserRepository.name))
  .addArgument(new Reference(PasswordHash.name))
  .addArgument(new Reference(TokenManager.name));
container
  .register(RegisterUsecase.name, RegisterUsecase)
  .addArgument(new Reference(UserRepository.name))
  .addArgument(new Reference(PasswordHash.name))
  .addArgument(new Reference(TokenManager.name));

container
  .register(CreateEmployeeUsecase.name, CreateEmployeeUsecase)
  .addArgument(new Reference(EmployeeRepository.name));
container
  .register(DeleteEmployeeUsecase.name, DeleteEmployeeUsecase)
  .addArgument(new Reference(EmployeeRepository.name));
container
  .register(GetEmployeeByIdUsecase.name, GetEmployeeByIdUsecase)
  .addArgument(new Reference(EmployeeRepository.name));
container
  .register(GetEmployeesUsecase.name, GetEmployeesUsecase)
  .addArgument(new Reference(EmployeeRepository.name));
container
  .register(GetEmployeesPaginationUsecase.name, GetEmployeesPaginationUsecase)
  .addArgument(new Reference(EmployeeRepository.name));
container
  .register(UpdateEmployeeUsecase.name, UpdateEmployeeUsecase)
  .addArgument(new Reference(EmployeeRepository.name));

container
  .register(CreateSeedUsecase.name, CreateSeedUsecase)
  .addArgument(new Reference(SeedRepository.name));
container
  .register(DeleteSeedUsecase.name, DeleteSeedUsecase)
  .addArgument(new Reference(SeedRepository.name));
container
  .register(GetSeedByIdUsecase.name, GetSeedByIdUsecase)
  .addArgument(new Reference(SeedRepository.name));
container
  .register(GetSeedsUsecase.name, GetSeedsUsecase)
  .addArgument(new Reference(SeedRepository.name));
container
  .register(GetSeedsPaginationUsecase.name, GetSeedsPaginationUsecase)
  .addArgument(new Reference(SeedRepository.name));
container
  .register(UpdateSeedUsecase.name, UpdateSeedUsecase)
  .addArgument(new Reference(SeedRepository.name));

container
  .register(CreateBlockUsecase.name, CreateBlockUsecase)
  .addArgument(new Reference(BlockRepository.name));
container
  .register(DeleteBlockUsecase.name, DeleteBlockUsecase)
  .addArgument(new Reference(BlockRepository.name));
container
  .register(GetBlockByIdUsecase.name, GetBlockByIdUsecase)
  .addArgument(new Reference(BlockRepository.name));
container
  .register(GetBlocksUsecase.name, GetBlocksUsecase)
  .addArgument(new Reference(BlockRepository.name));
container
  .register(GetBlocksPaginationUsecase.name, GetBlocksPaginationUsecase)
  .addArgument(new Reference(BlockRepository.name));
container
  .register(UpdateBlockUsecase.name, UpdateBlockUsecase)
  .addArgument(new Reference(BlockRepository.name));

container
  .register(CreateConditionUsecase.name, CreateConditionUsecase)
  .addArgument(new Reference(ConditionRepository.name));
container
  .register(DeleteConditionUsecase.name, DeleteConditionUsecase)
  .addArgument(new Reference(ConditionRepository.name));
container
  .register(GetConditionByIdUsecase.name, GetConditionByIdUsecase)
  .addArgument(new Reference(ConditionRepository.name));
container
  .register(GetConditionsUsecase.name, GetConditionsUsecase)
  .addArgument(new Reference(ConditionRepository.name));
container
  .register(GetConditionsPaginationUsecase.name, GetConditionsPaginationUsecase)
  .addArgument(new Reference(ConditionRepository.name));
container
  .register(UpdateConditionUsecase.name, UpdateConditionUsecase)
  .addArgument(new Reference(ConditionRepository.name));

container
  .register(CreatePlantUsecase.name, CreatePlantUsecase)
  .addArgument(new Reference(PlantRepository.name));
container
  .register(DeletePlantUsecase.name, DeletePlantUsecase)
  .addArgument(new Reference(PlantRepository.name));
container
  .register(GetPlantByIdUsecase.name, GetPlantByIdUsecase)
  .addArgument(new Reference(PlantRepository.name));
container
  .register(GetPlantsUsecase.name, GetPlantsUsecase)
  .addArgument(new Reference(PlantRepository.name));
container
  .register(GetPlantsPaginationUsecase.name, GetPlantsPaginationUsecase)
  .addArgument(new Reference(PlantRepository.name));
container
  .register(GetPlantStatisticUsecase.name, GetPlantStatisticUsecase)
  .addArgument(new Reference(PlantRepository.name));
container
  .register(UpdatePlantUsecase.name, UpdatePlantUsecase)
  .addArgument(new Reference(PlantRepository.name));

container
  .register(CreateHarvestUsecase.name, CreateHarvestUsecase)
  .addArgument(new Reference(HarvestRepository.name));
container
  .register(DeleteHarvestUsecase.name, DeleteHarvestUsecase)
  .addArgument(new Reference(HarvestRepository.name));
container
  .register(GetHarvestByIdUsecase.name, GetHarvestByIdUsecase)
  .addArgument(new Reference(HarvestRepository.name));
container
  .register(GetHarvestsUsecase.name, GetHarvestsUsecase)
  .addArgument(new Reference(HarvestRepository.name));
container
  .register(GetHarvestsPaginationUsecase.name, GetHarvestsPaginationUsecase)
  .addArgument(new Reference(HarvestRepository.name));
container
  .register(GetHarvestStatisticUsecase.name, GetHarvestStatisticUsecase)
  .addArgument(new Reference(HarvestRepository.name));
container
  .register(UpdateHarvestUsecase.name, UpdateHarvestUsecase)
  .addArgument(new Reference(HarvestRepository.name));

container
  .register(CreatePresenceUsecase.name, CreatePresenceUsecase)
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(DeletePresenceUsecase.name, DeletePresenceUsecase)
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(GetPresenceByIdUsecase.name, GetPresenceByIdUsecase)
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(GetPresencesUsecase.name, GetPresencesUsecase)
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(GetPresencesPaginationUsecase.name, GetPresencesPaginationUsecase)
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(GetPresenceStatisticUsecase.name, GetPresenceStatisticUsecase)
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(
    GetPresenceStatisticForExcelUsecase.name,
    GetPresenceStatisticForExcelUsecase
  )
  .addArgument(new Reference(PresenceRepository.name));
container
  .register(UpsertPresenceUsecase.name, UpsertPresenceUsecase)
  .addArgument(new Reference(PresenceRepository.name));

export { container };
