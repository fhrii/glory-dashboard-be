import Joi from 'joi';

const GetEmployeeUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetEmployeeByIdUsecase {
  constructor(employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.employeeRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getEmployeeUsecase = GetEmployeeUsecaseSchema.validate(payload);

    if (getEmployeeUsecase.error) {
      throw new Error(
        `GET_EMPLOYEE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getEmployeeUsecase.error.message}`
      );
    }
  }
}
