export class HarvestRepository {
  async create(_harvest) {
    throw new Error('HARVEST_REPOSITORY.CREATE.METHOD_NOT_IMPLEMENTED');
  }

  async findOneById(_id) {
    throw new Error('HARVEST_REPOSITORY.FIND_ONE_BY_ID.METHOD_NOT_IMPLEMENTED');
  }

  async findMany(_condition, _options) {
    throw new Error('HARVEST_REPOSITORY.FIND_MANY.METHOD_NOT_IMPLEMENTED');
  }

  async findPagination(_condition, _options) {
    throw new Error(
      'HARVEST_REPOSITORY.FIND_PAGINATION.METHOD_NOT_IMPLEMENTED'
    );
  }

  async update(_id, _harvest) {
    throw new Error('HARVEST_REPOSITORY.UPDATE.METHOD_NOT_IMPLEMENTED');
  }

  async delete(_id) {
    throw new Error('HARVEST_REPOSITORY.DELETE.METHOD_NOT_IMPLEMENTED');
  }

  async statistic(_condition) {
    throw new Error('HARVEST_REPOSITORY.STATISTIC.METHOD_NOT_IMPLEMENTED');
  }
}
