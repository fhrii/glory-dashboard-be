import { ClientError } from './ClientError';

export class AuthorizationError extends ClientError {
  constructor(message) {
    super(403, message);

    this.name = 'AuthenticationError';
  }
}
