import { protectedRoute } from '@/Infrastructures/middlewares';

import { PlantHandler } from './handler';

export function plantRoutes(container, router, passport, asyncHandler) {
  const plantHandler = new PlantHandler(container);

  router.get(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.getPlantsHandler(...args))
  );
  router.post(
    '/',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.postPlantHandler(...args))
  );
  router.get(
    '/statistic',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.getPlantStatistic(...args))
  );
  router.get(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.getPlantHandler(...args))
  );
  router.put(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.putPlantHandler(...args))
  );
  router.patch(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.patchPlantHandler(...args))
  );
  router.delete(
    '/:id',
    (...args) => protectedRoute(...args, passport),
    asyncHandler((...args) => plantHandler.deletePlantHandler(...args))
  );

  return ['/plants', router];
}
