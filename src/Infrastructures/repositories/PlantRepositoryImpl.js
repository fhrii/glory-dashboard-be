import { Prisma } from '@prisma/client';

import { BadRequestError, NotFoundError } from '@/common/exceptions';
import { Plant, PlantStatistic } from '@/Domain/Plant';
import { Seed } from '@/Domain/Seed';
import {
  dayjs,
  parseDayJsToDateWithoutTime,
  parseStringToDateWithoutTime,
} from '@/utils';

export class PlantRepositoryImpl {
  constructor(prisma, seedRepository, blockRepository) {
    this.prisma = prisma;
    this.seedRepository = seedRepository;
    this.blockRepository = blockRepository;
  }

  async create({ plant, date, seedId, blockId }) {
    try {
      const plantEntity = await this.prisma.plant.create({
        data: {
          plant,
          date,
          seed: { connect: { id: seedId } },
          block: { connect: { id: blockId } },
        },
        include: { seed: true, block: true },
      });

      return this.plantEntityToPlant(plantEntity);
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new BadRequestError(
            `Plant with date '${date}', seedId '${seedId}', and blockId '${blockId}' is already exist`
          );
        } else if (error.code === 'P2025') {
          const errorMessage = error.message;

          if (errorMessage.includes("'Seed' record(s)"))
            throw new NotFoundError(`Seed with id '${seedId}' was not found`);
          if (errorMessage.includes("'Block' record(s)"))
            throw new NotFoundError(`Block with id '${blockId}' was not found`);

          throw error;
        }
      }

      throw error;
    }
  }

  async findOneById(id) {
    const plantEntity = await this.prisma.plant.findUnique({
      where: { id },
      include: { seed: true, block: true },
    });

    if (!plantEntity)
      throw new NotFoundError(`Plant with id '${id}' was not found`);

    return this.plantEntityToPlant(plantEntity);
  }

  async findMany({ seed, block, plant, startDate, endDate }, { skip, take }) {
    const plantEntities = await this.prisma.plant.findMany({
      where: {
        plant,
        seed: { name: { contains: seed } },
        block: { name: { contains: block } },
        date: {
          gte: startDate && parseStringToDateWithoutTime(startDate),
          lte: endDate && parseStringToDateWithoutTime(endDate),
        },
      },
      skip,
      take,
      orderBy: {
        date: 'desc',
      },
      include: { seed: true, block: true },
    });

    return this.plantEntitiesToPlants(plantEntities);
  }

  async findPagination(
    { seed, block, plant, startDate, endDate },
    { skip = 0, take }
  ) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const totalPlants = await this.prisma.plant.count({
      where: {
        plant,
        seed: { name: { contains: seed } },
        block: { name: { contains: block } },
        date: {
          gte: startDate && parseStringToDateWithoutTime(startDate),
          lte: endDate && parseStringToDateWithoutTime(endDate),
        },
      },
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalPlants / take);

    return { currentPage, totalPages };
  }

  async update(id, { plant, date, seedId, blockId }) {
    try {
      const plantEntity = await this.prisma.plant.update({
        where: { id },
        data: {
          plant,
          date,
          seed: seedId ? { connect: { id: seedId } } : undefined,
          block: blockId ? { connect: { id: blockId } } : undefined,
        },
        include: { seed: true, block: true },
      });

      return this.plantEntityToPlant(plantEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        const errorMessage = error.message.toLowerCase();

        if (errorMessage.includes("'Seed' record(s)"))
          throw new NotFoundError(`Seed with id '${seedId}' was not found`);
        if (errorMessage.includes("'Block' record(s)"))
          throw new NotFoundError(`Block with id '${blockId}' was not found`);
        throw new NotFoundError(`Plant with id '${id}' was not found`);
      }

      throw error;
    }
  }

  async delete(id) {
    try {
      await this.prisma.plant.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Plant with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  async statistic({ startDate, endDate }) {
    let plantDateGte;
    let plantDateLte;

    if (startDate) plantDateGte = parseStringToDateWithoutTime(startDate);
    if (endDate) plantDateLte = parseStringToDateWithoutTime(endDate);
    if (!startDate && !endDate) {
      const today = dayjs();
      const daySevenDaysBeforeToday = today.subtract(7, 'day');
      plantDateGte = parseDayJsToDateWithoutTime(daySevenDaysBeforeToday);
    }

    const plantGroupByDateAndSeedEntities = await this.prisma.plant.groupBy({
      where: { date: { gte: plantDateGte, lte: plantDateLte } },
      by: ['date', 'seedId'],
      orderBy: {
        date: 'desc',
      },
      _sum: {
        plant: true,
      },
    });
    const allDate = plantGroupByDateAndSeedEntities
      .filter(
        (plantGroupByDateAndSeedEntity, index) =>
          plantGroupByDateAndSeedEntities.findIndex(
            (aPlantGroupByDateAndSeedEntity) =>
              aPlantGroupByDateAndSeedEntity.date.getTime() ===
              plantGroupByDateAndSeedEntity.date.getTime()
          ) === index
      )
      .map((group) => group.date);
    const seedIds = plantGroupByDateAndSeedEntities
      .filter(
        (plantGroupByDateAndSeedEntity, index) =>
          plantGroupByDateAndSeedEntities.findIndex(
            (aPlantGroupByDateAndSeedEntity) =>
              aPlantGroupByDateAndSeedEntity.seedId ===
              plantGroupByDateAndSeedEntity.seedId
          ) === index
      )
      .map((group) => group.seedId);
    const seedEntities = await this.prisma.seed.findMany({
      where: { id: { in: seedIds } },
    });
    const seeds = seedEntities.map(
      (seedEntity) =>
        new Seed({
          id: seedEntity.id,
          name: seedEntity.name,
          status: seedEntity.status,
          createdAt: seedEntity.createdAt,
          updatedAt: seedEntity.updatedAt,
        })
    );
    return allDate.map((date) => {
      const filteredPlantGroupByDateAndSeedEntities =
        plantGroupByDateAndSeedEntities.filter(
          (plantGroupByDateAndSeedEntity) =>
            plantGroupByDateAndSeedEntity.date.getTime() === date.getTime()
        );
      const seedsStatistic = filteredPlantGroupByDateAndSeedEntities.map(
        (plantGroupByDateAndSeedEntity) => ({
          seed: seeds.find(
            (seed) => seed.id === plantGroupByDateAndSeedEntity.seedId
          ),
          plant: plantGroupByDateAndSeedEntity._sum.plant,
        })
      );

      return new PlantStatistic({ date, seeds: seedsStatistic });
    });
  }

  plantEntityToPlant({
    id,
    seed: seedEntity,
    block: blockEntity,
    plant,
    date,
    createdAt,
    updatedAt,
  }) {
    const seed = this.seedRepository.seedEntityToSeed(seedEntity);
    const block = this.blockRepository.blockEntityToBlock(blockEntity);

    return new Plant({
      id,
      seed,
      block,
      plant,
      date,
      createdAt,
      updatedAt,
    });
  }

  plantEntitiesToPlants(plantEntities) {
    return plantEntities.map((plantEntity) =>
      this.plantEntityToPlant(plantEntity)
    );
  }
}
