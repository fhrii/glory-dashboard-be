export class ExcelImpl {
  constructor(writeXlsxFile) {
    this.writeXlsxFile = writeXlsxFile;
  }

  async write(data, { columns, fontSize, schema } = {}) {
    return this.writeXlsxFile(data, { columns, fontSize, schema });
  }
}
