import { BadRequestError } from './BadRequestError';

export const DomainErrorTranslator = {
  translate: (error) =>
    DomainErrorTranslator.directories[error.message] || error,
};

DomainErrorTranslator.directories = {
  'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.USERNAME_STRING':
    new BadRequestError('username must be a string'),
  'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.USERNAME_ALPHANUM':
    new BadRequestError('username must be a alphanumeric'),
  'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.USERNAME_REQUIRED':
    new BadRequestError('username cannot be empty'),
  'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.PASSWORD_STRING':
    new BadRequestError('password must be a string'),
  'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.PASSWORD_REQUIRED':
    new BadRequestError('password cannot be empty'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_REQUIRED':
    new BadRequestError('name cannot be empty'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_REQUIRED':
    new BadRequestError('status cannot be empty'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_STRING':
    new BadRequestError('gender must be a string'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_VALID':
    new BadRequestError('gender must be one of MAN or WOMAN'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_REQUIRED':
    new BadRequestError('gender cannot be empty'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_NUMBER':
    new BadRequestError('age must be a number'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_INTEGER':
    new BadRequestError('age must be an integer number'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_POSITIVE':
    new BadRequestError('age must be a positive number'),
  'CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_REQUIRED':
    new BadRequestError('age cannot be empty'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be a alphanumeric'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_STRING':
    new BadRequestError('gender must be a string'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_VALID':
    new BadRequestError('gender must be one of MAN or WOMAN'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_NUMBER':
    new BadRequestError('age must be a number'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_INTEGER':
    new BadRequestError('age must be an integer number'),
  'UPDATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.AGE_POSITIVE':
    new BadRequestError('age must be a positive number'),
  'CREATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'CREATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_REQUIRED':
    new BadRequestError('name cannot be empty'),
  'CREATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'CREATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_REQUIRED':
    new BadRequestError('status cannot be empty'),
  'UPDATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING': new BadRequestError(
    'id must be a string'
  ),
  'UPDATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be a alphanumeric'),
  'UPDATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('status must be a string'),
  'UPDATE_SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'CREATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_REQUIRED':
    new BadRequestError('name cannot be empty'),
  'CREATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'CREATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_REQUIRED':
    new BadRequestError('status cannot be empty'),
  'UPDATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'UPDATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be a alphanumeric'),
  'UPDATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'UPDATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'UPDATE_BLOCK.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'CREATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_REQUIRED':
    new BadRequestError('name cannot be empty'),
  'CREATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'CREATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_REQUIRED':
    new BadRequestError('status cannot be empty'),
  'UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be a alphanumeric'),
  'UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'UPDATE_CONDITION.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of ACTIVE or NOT_ACTIVE'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_STRING':
    new BadRequestError('seedId must be a string'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_ALPHANUM':
    new BadRequestError('seedId must be an alphanumeric'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_REQUIRED':
    new BadRequestError('seedId cannot be empty'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_STRING':
    new BadRequestError('blockId must be a string'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_ALPHANUM':
    new BadRequestError('blockId must be an alphanumeric'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_REQUIRED':
    new BadRequestError('blockId cannot be empty'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_NUMBER':
    new BadRequestError('plant must be a number'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_INTEGER':
    new BadRequestError('plant must be an integer number'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_POSITIVE':
    new BadRequestError('plant must be a positive number'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_REQUIRED':
    new BadRequestError('plant cannot be empty'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_DATE':
    new BadRequestError('date must be a date'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_FORMAT':
    new BadRequestError('date must be YYYY-MM-DD'),
  'CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_REQUIRED':
    new BadRequestError('date cannot be empty'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_STRING':
    new BadRequestError('seedId must be a string'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_ALPHANUM':
    new BadRequestError('seedId must be an alphanumeric'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_STRING':
    new BadRequestError('blockId must be a string'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_ALPHANUM':
    new BadRequestError('blockId must be an alphanumeric'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_NUMBER':
    new BadRequestError('plant must be a number'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_INTEGER':
    new BadRequestError('plant must be an integer number'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_POSITIVE':
    new BadRequestError('plant must be a positive number'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_DATE':
    new BadRequestError('date must be a date'),
  'UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_FORMAT':
    new BadRequestError('date format must be YYYY-MM-DD'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_STRING':
    new BadRequestError('seedId must be a string'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_ALPHANUM':
    new BadRequestError('seedId must be an alphanumeric'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_REQUIRED':
    new BadRequestError('seedId cannot be empty'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_STRING':
    new BadRequestError('blockId must be a string'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_ALPHANUM':
    new BadRequestError('blockId must be an alphanumeric'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_REQUIRED':
    new BadRequestError('blockId cannot be empty'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_ID_STRING':
    new BadRequestError('conditionId must be a string'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_ID_ALPHANUM':
    new BadRequestError('conditionId must be an alphanumeric'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_ID_REQUIRED':
    new BadRequestError('conditionId cannot be empty'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_NUMBER':
    new BadRequestError('harvest must be a number'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_INTEGER':
    new BadRequestError('harvest must be an integer number'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_POSITIVE':
    new BadRequestError('harvest must be a positive number'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_REQUIRED':
    new BadRequestError('harvest cannot be empty'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_DATE':
    new BadRequestError('date must be a date'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_FORMAT':
    new BadRequestError('date format must be YYYY-MM-DD'),
  'CREATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_REQUIRED':
    new BadRequestError('date cannot be empty'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_STRING':
    new BadRequestError('seedId must be a string'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_ID_ALPHANUM':
    new BadRequestError('seedId must be an alphanumeric'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_STRING':
    new BadRequestError('blockId must be a string'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_ID_ALPHANUM':
    new BadRequestError('blockId must be an alphanumeric'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_ID_STRING':
    new BadRequestError('conditionId must be a string'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_ID_ALPHANUM':
    new BadRequestError('conditionId must be an alphanumeric'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_NUMBER':
    new BadRequestError('harvest must be a number'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_INTEGER':
    new BadRequestError('harvest must be an integer number'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_POSITIVE':
    new BadRequestError('harvest must be a positive number'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_DATE':
    new BadRequestError('date must be a date'),
  'UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_FORMAT':
    new BadRequestError('date format must be YYYY-MM-DD'),
  'CREATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_STRING':
    new BadRequestError('employeeId must be a string'),
  'CREATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_ALPHANUM':
    new BadRequestError('employeeId must be an alphanumeric'),
  'CREATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_REQUIRED':
    new BadRequestError('employeeId cannot be empty'),
  'CREATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_STRING':
    new BadRequestError('presenceType must be a string'),
  'CREATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_VALID':
    new BadRequestError(
      'presenceType must be one of ABSENT, PERMIT, SICK or PRESENCE'
    ),
  'CREATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_REQUIRED':
    new BadRequestError('presenceType cannot be empty'),
  'UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_STRING':
    new BadRequestError('employeeId must be a string'),
  'UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_ALPHANUM':
    new BadRequestError('employeeId must be an alphanumeric'),
  'UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_REQUIRED':
    new BadRequestError('employeeId cannot be empty'),
  'UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_STRING':
    new BadRequestError('presenceType must be a string'),
  'UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_VALID':
    new BadRequestError(
      'presenceType must be one of ABSENT, PERMIT, SICK or PRESENCE'
    ),
  'UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_REQUIRED':
    new BadRequestError('presenceType cannot be empty'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_DATE':
    new BadRequestError('date must be a date'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_FORMAT':
    new BadRequestError('date format must be YYYY-MM-DD'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_REQUIRED':
    new BadRequestError('date cannot be empty'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TIME_STRING':
    new BadRequestError('presenceTime must be a string'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TIME_VALID':
    new BadRequestError('presenceTime must be one of MORNING or NOUN'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TIME_REQUIRED':
    new BadRequestError('presenceTime cannot be empty'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEES_PRESENCE_ARRAY':
    new BadRequestError('employeesPresence must be an array'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_STRING':
    new BadRequestError('employeeId must be a string'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_ALPHANUM':
    new BadRequestError('employeeId must be an alphanumeric'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_REQUIRED':
    new BadRequestError('employeeId cannot be empty'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_STRING':
    new BadRequestError('presenceType must be a string'),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_VALID':
    new BadRequestError(
      'presenceType must be one of ABSENT, PERMIT, SICK or PRESENCE'
    ),
  'CREATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_REQUIRED':
    new BadRequestError('presenceType cannot be empty'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TIME_STRING':
    new BadRequestError('presenceTime must be a string'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TIME_VALID':
    new BadRequestError('presenceTime must be one of MORNING or NOUN'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TIME_REQUIRED':
    new BadRequestError('presenceTime cannot be empty'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEES_PRESENCE_ARRAY':
    new BadRequestError('employeesPresence must be an array'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEES_PRESENCE_MIN':
    new BadRequestError('employeesPresence must have at least 1 item'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEES_PRESENCE_REQUIRED':
    new BadRequestError('employeesPresence cannot be empty'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_STRING':
    new BadRequestError('employeeId must be a string'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_ALPHANUM':
    new BadRequestError('employeeId must be an alphanumeric'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.EMPLOYEE_ID_REQUIRED':
    new BadRequestError('employeeId cannot be empty'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_STRING':
    new BadRequestError('presenceType must be a string'),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_VALID':
    new BadRequestError(
      'presenceType must be one of ABSENT, PERMIT, SICK or PRESENCE'
    ),
  'UPDATE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.PRESENCE_TYPE_REQUIRED':
    new BadRequestError('presenceType cannot be empty'),

  'GET_BLOCK_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_BLOCK_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_BLOCK_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_BLOCKS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_BLOCKS_PAGINATION_USECASEGET_BLOCKS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_CONDITION_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_CONDITION_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_CONDITION_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_CONDITIONS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_CONDITIONS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_SEED_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_SEED_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_SEED_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_SEEDS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_SEEDS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_EMPLOYEE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_EMPLOYEE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_EMPLOYEE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_STRING':
    new BadRequestError('gender must be a string'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_VALID':
    new BadRequestError('status must be one of MAN or WOMAN'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.NAME_STRING':
    new BadRequestError('name must be a string'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_STRING':
    new BadRequestError('status must be a string'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.STATUS_VALID':
    new BadRequestError('status must be one of 1 or 0'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_STRING':
    new BadRequestError('gender must be a string'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.GENDER_VALID':
    new BadRequestError('status must be one of MAN or WOMAN'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_EMPLOYEES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_HARVEST_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_HARVEST_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_HARVEST_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_NUMBER':
    new BadRequestError('harvest must be a number'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_INTEGER':
    new BadRequestError('harvest must be an integer'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_MIN':
    new BadRequestError('harvest must be equal or greater than 0'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_STRING':
    new BadRequestError('seed must be a string'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_STRING':
    new BadRequestError('block must be a string'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_STRING':
    new BadRequestError('condition must be a string'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_HARVESTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_NUMBER':
    new BadRequestError('harvest must be a number'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_INTEGER':
    new BadRequestError('harvest must be an integer'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.HARVEST_MIN':
    new BadRequestError('harvest must be equal or greater than 0'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_STRING':
    new BadRequestError('seed must be a string'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_STRING':
    new BadRequestError('block must be a string'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.CONDITION_STRING':
    new BadRequestError('condition must be a string'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_HARVESTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_HARVEST_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_HARVEST_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_PLANT_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_PLANT_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_PLANT_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_NUMBER':
    new BadRequestError('plant must be a number'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_INTEGER':
    new BadRequestError('plant must be an integer'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_MIN':
    new BadRequestError('plant must be equal or greater than 0'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_STRING':
    new BadRequestError('seed must be a string'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_STRING':
    new BadRequestError('block must be a string'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_NUMBER':
    new BadRequestError('plant must be a number'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_INTEGER':
    new BadRequestError('plant must be an integer'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.PLANT_MIN':
    new BadRequestError('plant must be equal or greater than 0'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SEED_STRING':
    new BadRequestError('seed must be a string'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.BLOCK_STRING':
    new BadRequestError('block must be a string'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_PLANTS_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_PLANT_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_PLANT_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_PRESENCE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_STRING':
    new BadRequestError('id must be a string'),
  'GET_PRESENCE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_ALPHANUM':
    new BadRequestError('id must be an alphanumeric'),
  'GET_PRESENCE_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.ID_REQUIRED':
    new BadRequestError('id cannot be empty'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_DATE':
    new BadRequestError('date must be a date'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.DATE_FORMAT':
    new BadRequestError('date format must be YYYY-MM-DD'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_PRESENCES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_NUMBER':
    new BadRequestError('skip must be a number'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_INTEGER':
    new BadRequestError('skip must be an integer'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.SKIP_MIN':
    new BadRequestError('skip must be greater than 0'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_NUMBER':
    new BadRequestError('take must be a number'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_INTEGER':
    new BadRequestError('take must be an integer'),
  'GET_PRESENCES_PAGINATION_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.TAKE_POSITIVE':
    new BadRequestError('take must be a positive number'),
  'GET_PRESENCE_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.START_DATE_FORMAT':
    new BadRequestError('startDate format must be YYYY-MM-DD'),
  'GET_PRESENCE_STATISTIC_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.END_DATE_FORMAT':
    new BadRequestError('endDate format must be YYYY-MM-DD'),
};
