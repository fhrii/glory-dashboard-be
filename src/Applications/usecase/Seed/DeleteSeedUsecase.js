export class DeleteSeedUsecase {
  constructor(seedRepository) {
    this.seedRepository = seedRepository;
  }

  async execute({ id }) {
    return this.seedRepository.delete(id);
  }
}
