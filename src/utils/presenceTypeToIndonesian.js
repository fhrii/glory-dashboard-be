export function presenceTypeToIndonesian(presenceType) {
  if (presenceType === 'PRESENCE') return 'Hadir';
  if (presenceType === 'ABSENT') return 'Tidak Hadir';
  if (presenceType === 'PERMIT') return 'Izin';
  if (presenceType === 'SICK') return 'Sakit';

  return presenceType;
}
