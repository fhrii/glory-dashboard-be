import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const GetPlantsUsecaseSchema = Joi.object({
  plant: Joi.number().integer().min(0).messages({
    'number.base': 'PLANT_NUMBER',
    'number.integer': 'PLANT_INTEGER',
    'number.min': 'PLANT_MIN',
  }),
  seed: Joi.string().allow('').messages({
    'string.base': 'SEED_STRING',
  }),
  block: Joi.string().allow('').messages({
    'string.base': 'BLOCK_STRING',
  }),
  startDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'START_DATE_DATE',
    'date.format': 'START_DATE_FORMAT',
  }),
  endDate: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'END_DATE_DATE',
    'date.format': 'END_DATE_FORMAT',
  }),
  skip: Joi.number().integer().min(0).messages({
    'number.base': 'SKIP_NUMBER',
    'number.integer': 'SKIP_INTEGER',
    'number.min': 'SKIP_MIN',
  }),
  take: Joi.number().integer().positive().messages({
    'number.base': 'TAKE_NUMBER',
    'number.integer': 'TAKE_INTEGER',
    'number.positive': 'TAKE_POSITIVE',
  }),
}).options({ allowUnknown: true });

export class GetPlantsUsecase {
  constructor(plantRepository) {
    this.plantRepository = plantRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { plant, seed, block, startDate, endDate, skip, take } =
      usecasePayload;
    return this.plantRepository.findMany(
      { plant, seed, block, startDate, endDate },
      { skip, take }
    );
  }

  verifyPayload(payload) {
    const getPlantsUsecase = GetPlantsUsecaseSchema.validate(payload);

    if (getPlantsUsecase.error) {
      throw new Error(
        `GET_PLANTS_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getPlantsUsecase.error.message}`
      );
    }
  }
}
