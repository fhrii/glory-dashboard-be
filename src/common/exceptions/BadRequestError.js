import { ClientError } from './ClientError';

export class BadRequestError extends ClientError {
  constructor(message) {
    super(400, message);

    this.name = 'BadRequestError';
  }
}
