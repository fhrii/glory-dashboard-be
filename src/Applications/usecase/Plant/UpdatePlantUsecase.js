import { UpdatePlant } from '@/Domain/Plant';

export class UpdatePlantUsecase {
  constructor(plantRepository) {
    this.plantRepository = plantRepository;
  }

  async execute(usecasePayload) {
    const { id, ...plantData } = new UpdatePlant(usecasePayload);
    return this.plantRepository.update(id, plantData);
  }
}
