import Joi from 'joi';

export const SeedSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  name: Joi.string().required().messages({
    'string.base': 'NAME_STRING',
    'string.alphanum': 'NAME_ALPHANUM',
    'any.required': 'NAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class Seed {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.name = payload.name;
    this.status = payload.status;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const seed = SeedSchema.validate(payload);

    if (seed.error) {
      throw new Error(
        `SEED.NOT_MEET_DATA_TYPE_SPECIFICATION.${seed.error.message}`
      );
    }
  }
}
