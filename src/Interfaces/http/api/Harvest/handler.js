import {
  CreateHarvestUsecase,
  DeleteHarvestUsecase,
  GetHarvestByIdUsecase,
  GetHarvestsPaginationUsecase,
  GetHarvestStatisticUsecase,
  GetHarvestsUsecase,
  UpdateHarvestUsecase,
} from '@/Applications/usecase/Harvest';
import { ClientError } from '@/common/exceptions';
import { BlockPresentation } from '@/Domain/Block';
import { ConditionPresentation } from '@/Domain/Condition';
import {
  HarvestPresentation,
  HarvestStatisticPresentation,
} from '@/Domain/Harvest';
import { SeedPresentation } from '@/Domain/Seed';
import { parseIntOrNotNaN } from '@/utils';

export class HarvestHandler {
  constructor(container) {
    this.container = container;
  }

  async getHarvestHandler(req, res) {
    const harvest = await this.container
      .get(GetHarvestByIdUsecase.name)
      .execute(req.params);
    const harvestPresentation = this.harvestToHarvestPresentation(harvest);

    res.status(200).json({ harvest: harvestPresentation });
  }

  async getHarvestsHandler(req, res) {
    const { harvest, seed, block, condition, startDate, endDate, skip, take } =
      req.query;
    const data = {
      harvest: parseIntOrNotNaN(harvest),
      seed: seed?.toLowerCase(),
      block: block?.toLowerCase(),
      condition: condition?.toLowerCase(),
      startDate,
      endDate,
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [harvests, harvestsPagination] = await Promise.all([
      this.container.get(GetHarvestsUsecase.name).execute(data),
      this.container.get(GetHarvestsPaginationUsecase.name).execute(data),
    ]);
    const harvestPresentations = this.harvestsToHarvestPresentations(harvests);

    res
      .status(200)
      .json({ harvests: harvestPresentations, ...harvestsPagination });
  }

  async getHarvestStatistic(req, res) {
    const statistic = await this.container
      .get(GetHarvestStatisticUsecase.name)
      .execute(req.query);
    const statisticPresentation = statistic.map(
      ({ date, seeds }) =>
        new HarvestStatisticPresentation({
          date,
          seeds: seeds.map(({ seed, harvest }) => ({
            seed: new SeedPresentation({
              id: seed.id,
              name: seed.name,
              status: seed.status,
            }),
            harvest,
          })),
        })
    );

    res.status(200).json({ statistic: statisticPresentation });
  }

  async postHarvestHandler(req, res) {
    const harvest = await this.container
      .get(CreateHarvestUsecase.name)
      .execute(req.body);
    const harvestPresentation = this.harvestToHarvestPresentation(harvest);

    res.status(201).json({ harvest: harvestPresentation });
  }

  async putHarvestHandler(req, res) {
    const harvest = await this.container
      .get(UpdateHarvestUsecase.name)
      .execute({ ...req.params, ...req.body });
    const harvestPresentation = this.harvestToHarvestPresentation(harvest);

    res.status(200).json({ harvest: harvestPresentation });
  }

  async patchHarvestHandler(req, res) {
    return this.putHarvestHandler(req, res);
  }

  async deleteHarvestHandler(req, res) {
    try {
      await this.container.get(DeleteHarvestUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the harvest');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no harvest was deleted');
        return;
      }

      throw error;
    }
  }

  harvestToHarvestPresentation({ id, seed, block, condition, harvest, date }) {
    const seedPresentation = new SeedPresentation({
      id: seed.id,
      name: seed.name,
      status: seed.status,
    });
    const blockPresentation = new BlockPresentation({
      id: block.id,
      name: block.name,
      status: block.status,
    });
    const conditionPresentation = new ConditionPresentation({
      id: condition.id,
      name: condition.name,
      status: condition.status,
    });

    return new HarvestPresentation({
      id,
      seed: seedPresentation,
      block: blockPresentation,
      condition: conditionPresentation,
      harvest,
      date,
    });
  }

  harvestsToHarvestPresentations(harvests) {
    return harvests.map((harvest) =>
      this.harvestToHarvestPresentation(harvest)
    );
  }
}
