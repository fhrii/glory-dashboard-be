import Joi from 'joi';

import { SeedPresentationSchema } from '@/Domain/Seed';

const HarvestStatisticPresentationSchema = Joi.object({
  date: Joi.date().required().messages({}),
  seeds: Joi.array()
    .items(
      Joi.object({
        seed: SeedPresentationSchema,
        harvest: Joi.number().integer().min(0).required().messages({
          'number.base': 'HARVEST_NUMBER',
          'number.integer': 'HARVEST_INTEGER',
          'number.min': 'HARVEST_MIN',
          'any.required': 'HARVEST_REQUIRED',
        }),
      })
    )
    .messages({
      'array.base': 'STATISTIC_ARRAY',
    }),
});

export class HarvestStatisticPresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.date = payload.date;
    this.seeds = payload.seeds;
  }

  verifyPayload(payload) {
    const harvestStatisticPresentation =
      HarvestStatisticPresentationSchema.validate(payload);

    if (harvestStatisticPresentation.error) {
      throw new Error(
        `HARVEST_STATISTIC_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${harvestStatisticPresentation.error.message}`
      );
    }
  }
}
