import Joi from 'joi';

const UserLoginSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'USERNAME_STRING',
    'string.alphanum': 'USERNAME_ALPHANUM',
    'any.required': 'USERNAME_REQUIRED',
  }),
  username: Joi.string().required().messages({
    'string.base': 'USERNAME_STRING',
    'string.alphanum': 'USERNAME_ALPHANUM',
    'any.required': 'USERNAME_REQUIRED',
  }),
  createdAt: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
  updatedAt: Joi.date().required().messages({
    'date.base': 'UPDATED_AT_DATE',
    'any.required': 'UPDATED_AT_REQUIRED',
  }),
});

export class User {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.username = payload.username;
    this.createdAt = payload.createdAt;
    this.updatedAt = payload.updatedAt;
  }

  verifyPayload(payload) {
    const userLogin = UserLoginSchema.validate(payload);

    if (userLogin.error) {
      throw new Error(
        `USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION.${userLogin.error.message}`
      );
    }
  }
}
