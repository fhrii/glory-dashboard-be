import Joi from 'joi';

const GetSeedUsecaseSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
});

export class GetSeedByIdUsecase {
  constructor(seedRepository) {
    this.seedRepository = seedRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { id } = usecasePayload;
    return this.seedRepository.findOneById(id);
  }

  verifyPayload(payload) {
    const getSeedUsecase = GetSeedUsecaseSchema.validate(payload);

    if (getSeedUsecase.error) {
      throw new Error(
        `GET_SEED_BY_ID_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getSeedUsecase.error.message}`
      );
    }
  }
}
