import cors from 'cors';
import express from 'express';
import asyncHandler from 'express-async-handler';
import passport from 'passport';

import {
  authRoutes,
  blockRoutes,
  conditionRoutes,
  employeeRoutes,
  plantRoutes,
  presenceRoutes,
  seedRoutes,
} from '@/Interfaces/http/api';
import { harvestRoutes } from '@/Interfaces/http/api/Harvest';

import { errorHandler, httpLogger, responseTransformer } from '../middlewares';
import { jwtStrategy } from '../strategies';

export function createServer(container) {
  const app = express();

  app.use(express.json());
  app.use(cors());
  app.use(httpLogger);
  app.use(responseTransformer);

  passport.use(jwtStrategy);

  app.use(...authRoutes(container, express.Router(), asyncHandler));
  app.use(
    ...employeeRoutes(container, express.Router(), passport, asyncHandler)
  );
  app.use(...seedRoutes(container, express.Router(), passport, asyncHandler));
  app.use(...blockRoutes(container, express.Router(), passport, asyncHandler));
  app.use(
    ...conditionRoutes(container, express.Router(), passport, asyncHandler)
  );
  app.use(...plantRoutes(container, express.Router(), passport, asyncHandler));
  app.use(
    ...harvestRoutes(container, express.Router(), passport, asyncHandler)
  );
  app.use(
    ...presenceRoutes(container, express.Router(), passport, asyncHandler)
  );

  app.use(errorHandler);

  return app;
}
