import { CreateCondition } from '@/Domain/Condition';

export class CreateConditionUsecase {
  constructor(conditionRepository) {
    this.conditionRepository = conditionRepository;
  }

  async execute(usecasePayload) {
    const createCondition = new CreateCondition(usecasePayload);
    return this.conditionRepository.create(createCondition);
  }
}
