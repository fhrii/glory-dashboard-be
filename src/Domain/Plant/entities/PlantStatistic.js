import Joi from 'joi';

import { SeedSchema } from '@/Domain/Seed';

const PlantStatisticSchema = Joi.object({
  date: Joi.date().required().messages({}),
  seeds: Joi.array()
    .items(
      Joi.object({
        seed: SeedSchema,
        plant: Joi.number().integer().min(0).required().messages({
          'number.base': 'PLANT_NUMBER',
          'number.integer': 'PLANT_INTEGER',
          'number.min': 'PLANT_MIN',
          'any.required': 'PLANT_REQUIRED',
        }),
      })
    )
    .messages({
      'array.base': 'STATISTIC_ARRAY',
    }),
});

export class PlantStatistic {
  constructor(payload) {
    this.verifyPayload(payload);

    this.date = payload.date;
    this.seeds = payload.seeds;
  }

  verifyPayload(payload) {
    const plantStatistic = PlantStatisticSchema.validate(payload);

    if (plantStatistic.error) {
      throw new Error(
        `PLANT_STATISTIC.NOT_MEET_DATA_TYPE_SPECIFICATION.${plantStatistic.error.message}`
      );
    }
  }
}
