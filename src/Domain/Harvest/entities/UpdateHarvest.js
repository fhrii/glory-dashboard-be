import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const UpdateHarvestSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  seedId: Joi.string().alphanum().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
  }),
  blockId: Joi.string().alphanum().messages({
    'string.base': 'BLOCK_ID_STRING',
    'string.alphanum': 'BLOCK_ID_ALPHANUM',
  }),
  conditionId: Joi.string().alphanum().messages({
    'string.base': 'CONDITION_ID_STRING',
    'string.alphanum': 'CONDITION_ID_ALPHANUM',
  }),
  harvest: Joi.number().integer().positive().messages({
    'number.base': 'HARVEST_NUMBER',
    'number.integer': 'HARVEST_INTEGER',
    'number.positive': 'HARVEST_POSITIVE',
  }),
  date: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'DATE_DATE',
    'date.format': 'DATE_FORMAT',
  }),
}).options({ allowUnknown: true });

export class UpdateHarvest {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.seedId = payload.seedId;
    this.blockId = payload.blockId;
    this.conditionId = payload.conditionId;
    this.harvest = payload.harvest;
    this.date = new Date(payload.date);
  }

  verifyPayload(payload) {
    const updateHarvest = UpdateHarvestSchema.validate(payload);

    if (updateHarvest.error) {
      throw new Error(
        `UPDATE_HARVEST.NOT_MEET_DATA_TYPE_SPECIFICATION.${updateHarvest.error.message}`
      );
    }
  }
}
