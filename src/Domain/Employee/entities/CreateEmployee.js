import Joi from 'joi';

const CreateEmployeeSchema = Joi.object({
  name: Joi.string().required().messages({
    'string.base': 'NAME_STRING',
    'any.required': 'NAME_REQUIRED',
  }),
  status: Joi.string().valid('ACTIVE', 'NOT_ACTIVE').required().messages({
    'string.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
    'any.required': 'STATUS_REQUIRED',
  }),
  gender: Joi.string().valid('MAN', 'WOMAN').required().messages({
    'string.base': 'GENDER_STRING',
    'any.only': 'GENDER_VALID',
    'any.required': 'GENDER_REQUIRED',
  }),
  age: Joi.number().integer().positive().required().messages({
    'number.base': 'AGE_NUMBER',
    'number.integer': 'AGE_INTEGER',
    'number.positive': 'AGE_POSITIVE',
    'any.required': 'AGE_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class CreateEmployee {
  constructor(payload) {
    this.verifyPayload(payload);

    this.name = payload.name;
    this.status = payload.status;
    this.gender = payload.gender;
    this.age = payload.age;
  }

  verifyPayload(payload) {
    const createEmployee = CreateEmployeeSchema.validate(payload);

    if (createEmployee.error) {
      throw new Error(
        `CREATE_EMPLOYEE.NOT_MEET_DATA_TYPE_SPECIFICATION.${createEmployee.error.message}`
      );
    }
  }
}
