import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const UpdatePlantSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  seedId: Joi.string().alphanum().messages({
    'string.base': 'SEED_ID_STRING',
    'string.alphanum': 'SEED_ID_ALPHANUM',
  }),
  blockId: Joi.string().alphanum().messages({
    'string.base': 'BLOCK_ID_STRING',
    'string.alphanum': 'BLOCK_ID_ALPHANUM',
  }),
  plant: Joi.number().integer().messages({
    'number.base': 'PLANT_NUMBER',
    'number.integer': 'PLANT_INTEGER',
    'number.positive': 'PLANT_POSITIVE',
  }),
  date: Joi.date().format('YYYY-MM-DD').messages({
    'date.base': 'DATE_DATE',
    'date.format': 'DATE_FORMAT',
  }),
}).options({ allowUnknown: true });

export class UpdatePlant {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.seedId = payload.seedId;
    this.blockId = payload.blockId;
    this.plant = payload.plant;
    this.date = new Date(payload.date);
  }

  verifyPayload(payload) {
    const updatePlant = UpdatePlantSchema.validate(payload);

    if (updatePlant.error) {
      throw new Error(
        `UPDATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.${updatePlant.error.message}`
      );
    }
  }
}
