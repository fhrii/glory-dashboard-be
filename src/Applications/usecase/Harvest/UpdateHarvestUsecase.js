import { UpdateHarvest } from '@/Domain/Harvest';

export class UpdateHarvestUsecase {
  constructor(harvestRepository) {
    this.harvestRepository = harvestRepository;
  }

  async execute(usecasePayload) {
    const { id, ...harvestData } = new UpdateHarvest(usecasePayload);
    return this.harvestRepository.update(id, harvestData);
  }
}
