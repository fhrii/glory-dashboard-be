import { UpdatePresence } from '@/Domain/Presence';

export class UpsertPresenceUsecase {
  constructor(presenceRepository) {
    this.presenceRepository = presenceRepository;
  }

  async execute(usecasePayload) {
    const { id, ...presenceData } = new UpdatePresence(usecasePayload);
    return this.presenceRepository.upsert(id, presenceData);
  }
}
