import { Prisma } from '@prisma/client';

import { BadRequestError, NotFoundError } from '@/common/exceptions';
import { User } from '@/Domain/User';

export class UserRepositoryImpl {
  constructor(prisma) {
    this.prisma = prisma;
  }

  async create({ username, password }) {
    try {
      const userEntity = await this.prisma.user.create({
        data: {
          username,
          password,
        },
      });

      return this.userEntityToUser(userEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2002'
      ) {
        throw new BadRequestError('Username is already taken');
      }

      throw error;
    }
  }

  async findOneByUsername(username) {
    const userEntity = await this.prisma.user.findUnique({
      where: { username },
      select: {
        id: true,
        username: true,
        createdAt: true,
        updatedAt: true,
      },
    });

    if (!userEntity) throw new NotFoundError(`User '${username}' not found`);

    return this.userEntityToUser(userEntity);
  }

  async getPasswordByUsername(username) {
    const user = await this.prisma.user.findUnique({
      where: {
        username,
      },
      select: {
        password: true,
      },
    });

    if (!user) throw new NotFoundError(`User '${username}' not found`);

    return user.password;
  }

  userEntityToUser({ id, username, createdAt, updatedAt }) {
    return new User({
      id,
      username,
      createdAt,
      updatedAt,
    });
  }
}
