import { Prisma } from '@prisma/client';

import { NotFoundError } from '@/common/exceptions';
import { Employee } from '@/Domain/Employee';

export class EmployeeRepositoryImpl {
  constructor(prisma) {
    this.prisma = prisma;
  }

  async create({ name, status, gender, age }) {
    const employeeEntity = await this.prisma.employee.create({
      data: {
        name,
        status,
        gender,
        age,
      },
    });

    return this.employeeEntityToEmployee(employeeEntity);
  }

  async findOneById(id) {
    const employeeEntity = await this.prisma.employee.findUnique({
      where: { id },
    });

    if (!employeeEntity)
      throw new NotFoundError(`Employee with id '${id}' was not found`);

    return this.employeeEntityToEmployee(employeeEntity);
  }

  async findMany({ name, status, gender }, { skip, take } = {}) {
    const employeeEntities = await this.prisma.employee.findMany({
      where: { name: { contains: name }, status, gender },
      skip,
      take,
      orderBy: {
        name: 'asc',
      },
    });

    return this.employeeEntitiesToEmployees(employeeEntities);
  }

  async findPagination({ name, status, gender }, { skip = 0, take }) {
    if (!take) return { currentPage: 1, totalPages: 1 };

    const conditions = {};

    if (name) conditions.name = { contains: name };
    if (status) conditions.status = status;
    if (gender) conditions.gender = gender;

    const totalEmployees = await this.prisma.employee.count({
      where: Object.keys(conditions).length > 0 ? conditions : undefined,
    });
    const currentPage = Math.floor(skip / take) + 1;
    const totalPages = Math.ceil(totalEmployees / take);

    return { currentPage, totalPages };
  }

  async update(id, { name, status, gender, age }) {
    try {
      const employeeEntity = await this.prisma.employee.update({
        where: { id },
        data: {
          name,
          status,
          gender,
          age,
        },
      });

      return this.employeeEntityToEmployee(employeeEntity);
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Employee with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  async delete(id) {
    try {
      await this.prisma.employee.delete({ where: { id } });
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new NotFoundError(`Employee with id '${id}' was not found'`);
      }

      throw error;
    }
  }

  async validateActiveIds(ids) {
    const employeeEntities = await this.prisma.employee.findMany({
      where: {
        id: { in: ids },
        status: 'ACTIVE',
      },
      select: { id: true },
    });

    if (employeeEntities.length !== ids.length)
      throw new NotFoundError('Some id was not found');
  }

  employeeEntityToEmployee({
    id,
    name,
    status,
    gender,
    age,
    createdAt,
    updatedAt,
  }) {
    return new Employee({
      id,
      name,
      status,
      gender,
      age,
      createdAt,
      updatedAt,
    });
  }

  employeeEntitiesToEmployees(employeeEntities) {
    return employeeEntities.map((employeeEntity) =>
      this.employeeEntityToEmployee(employeeEntity)
    );
  }
}
