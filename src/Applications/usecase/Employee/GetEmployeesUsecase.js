import Joi from 'joi';

const GetEmployeesUsecaseSchema = Joi.object({
  name: Joi.string().allow('').messages({
    'string.base': 'NAME_STRING',
  }),
  status: Joi.number().valid(1, 0).messages({
    'number.base': 'STATUS_STRING',
    'any.only': 'STATUS_VALID',
  }),
  gender: Joi.string().valid('MAN', 'WOMAN').messages({
    'string.base': 'GENDER_STRING',
    'any.only': 'GENDER_VALID',
  }),
  skip: Joi.number().integer().min(0).messages({
    'number.base': 'SKIP_NUMBER',
    'number.integer': 'SKIP_INTEGER',
    'number.min': 'SKIP_MIN',
  }),
  take: Joi.number().integer().positive().messages({
    'number.base': 'TAKE_NUMBER',
    'number.integer': 'TAKE_INTEGER',
    'number.positive': 'TAKE_POSITIVE',
  }),
}).options({ allowUnknown: true });

export class GetEmployeesUsecase {
  constructor(employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  async execute(usecasePayload) {
    this.verifyPayload(usecasePayload);
    const { name, status, gender, skip, take } = usecasePayload;
    let translatedStatus;

    if (status === 1) translatedStatus = 'ACTIVE';
    else if (status === 0) translatedStatus = 'NOT_ACTIVE';

    return this.employeeRepository.findMany(
      { name, status: translatedStatus, gender },
      { skip, take }
    );
  }

  verifyPayload(payload) {
    const getConditionsUsecase = GetEmployeesUsecaseSchema.validate(payload);

    if (getConditionsUsecase.error) {
      throw new Error(
        `GET_EMPLOYEES_USECASE.NOT_MEET_DATA_TYPE_SPECIFICATION.${getConditionsUsecase.error.message}`
      );
    }
  }
}
