import { CreateHarvest } from '@/Domain/Harvest';

export class CreateHarvestUsecase {
  constructor(harvestRepository) {
    this.plantRepository = harvestRepository;
  }

  async execute(usecasePayload) {
    const createHarvest = new CreateHarvest(usecasePayload);
    return this.plantRepository.create(createHarvest);
  }
}
