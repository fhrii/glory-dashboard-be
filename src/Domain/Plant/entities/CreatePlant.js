import JoiDate from '@joi/date';
import JoiDefault from 'joi';

const Joi = JoiDefault.extend(JoiDate);

const CreatePlantSchema = Joi.object({
  seedId: Joi.string().alphanum().required().messages({
    'string.base': 'SEED_ID_STRING',
    'string.alphanum': 'SEED_ID_ALPHANUM',
    'any.required': 'SEED_ID_REQUIRED',
  }),
  blockId: Joi.string().alphanum().required().messages({
    'string.base': 'BLOCK_ID_STRING',
    'string.alphanum': 'BLOCK_ID_ALPHANUM',
    'any.required': 'BLOCK_ID_REQUIRED',
  }),
  plant: Joi.number().integer().positive().required().messages({
    'number.base': 'PLANT_NUMBER',
    'number.integer': 'PLANT_INTEGER',
    'number.positive': 'PLANT_POSITIVE',
    'any.required': 'PLANT_REQUIRED',
  }),
  date: Joi.date().format('YYYY-MM-DD').required().messages({
    'date.base': 'DATE_DATE',
    'date.format': 'DATE_FORMAT',
    'any.required': 'DATE_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class CreatePlant {
  constructor(payload) {
    this.verifyPayload(payload);

    this.seedId = payload.seedId;
    this.blockId = payload.blockId;
    this.plant = payload.plant;
    this.date = new Date(payload.date);
  }

  verifyPayload(payload) {
    const createPlant = CreatePlantSchema.validate(payload);

    if (createPlant.error) {
      throw new Error(
        `CREATE_PLANT.NOT_MEET_DATA_TYPE_SPECIFICATION.${createPlant.error.message}`
      );
    }
  }
}
