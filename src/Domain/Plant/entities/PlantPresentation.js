import Joi from 'joi';

import { BlockPresentationSchema } from '@/Domain/Block';
import { SeedPresentationSchema } from '@/Domain/Seed';

const PlantPresentationSchema = Joi.object({
  id: Joi.string().alphanum().required().messages({
    'string.base': 'ID_STRING',
    'string.alphanum': 'ID_ALPHANUM',
    'any.required': 'ID_REQUIRED',
  }),
  seed: SeedPresentationSchema,
  block: BlockPresentationSchema,
  plant: Joi.number().integer().positive().required().messages({
    'number.base': 'PLANT_NUMBER',
    'number.integer': 'PLANT_INTEGER',
    'number.positive': 'PLANT_POSITIVE',
    'any.required': 'PLANT_REQUIRED',
  }),
  date: Joi.date().required().messages({
    'date.base': 'CREATED_AT_DATE',
    'any.required': 'CREATED_AT_REQUIRED',
  }),
}).options({ allowUnknown: true });

export class PlantPresentation {
  constructor(payload) {
    this.verifyPayload(payload);

    this.id = payload.id;
    this.seed = payload.seed;
    this.block = payload.block;
    this.plant = payload.plant;
    this.date = payload.date;
  }

  verifyPayload(payload) {
    const plantPresentation = PlantPresentationSchema.validate(payload);

    if (plantPresentation.error) {
      throw new Error(
        `PLANT_PRESENTATION.NOT_MEET_DATA_TYPE_SPECIFICATION.${plantPresentation.error.message}`
      );
    }
  }
}
