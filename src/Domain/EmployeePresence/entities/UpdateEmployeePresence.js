import Joi from 'joi';

export const UpdateEmployeePresenceSchema = Joi.object({
  employeeId: Joi.string().alphanum().required().messages({
    'string.base': 'EMPLOYEE_ID_STRING',
    'string.alphanum': 'EMPLOYEE_ID_ALPHANUM',
    'any.required': 'EMPLOYEE_ID_REQUIRED',
  }),
  presenceType: Joi.string()
    .valid('ABSENT', 'PERMIT', 'SICK', 'PRESENCE')
    .required()
    .messages({
      'string.base': 'PRESENCE_TYPE_STRING',
      'any.only': 'PRESENCE_TYPE_VALID',
      'any.required': 'PRESENCE_TYPE_REQUIRED',
    }),
});

export class UpdateEmployeePresence {
  constructor(payload) {
    this.verifyPayload(payload);

    this.employeeId = payload.employeeId;
    this.presenceType = payload.presenceType;
  }

  verifyPayload(payload) {
    const updateEmployeePresence =
      UpdateEmployeePresenceSchema.validate(payload);

    if (updateEmployeePresence.error) {
      throw new Error(
        `UPDATE_EMPLOYEE_PRESENCE.NOT_MEET_DATA_TYPE_SPECIFICATION.${updateEmployeePresence.error.message}`
      );
    }
  }
}
