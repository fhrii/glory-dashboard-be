// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
}

enum Status {
  ACTIVE
  NOT_ACTIVE
}

enum Gender {
  MAN
  WOMAN
}

enum PresenceTime {
  MORNING
  NOUN
}

enum PresenceType {
  ABSENT
  PERMIT
  SICK
  PRESENCE
}

model User {
  id        String   @id @default(cuid())
  username  String   @unique
  password  String
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("users")
}

model Employee {
  id               String             @id @default(cuid())
  name             String
  status           Status             @default(NOT_ACTIVE)
  gender           Gender
  age              Int
  createdAt        DateTime           @default(now()) @map("created_at")
  updatedAt        DateTime           @updatedAt @map("updated_at")
  EmployeePresence EmployeePresence[]

  @@map("employees")
}

model Seed {
  id        String    @id @default(cuid())
  name      String    @unique
  status    Status    @default(NOT_ACTIVE)
  createdAt DateTime  @default(now()) @map("created_at")
  updatedAt DateTime  @updatedAt @map("updated_at")
  plant     Plant[]
  harvest   Harvest[]

  @@map("seeds")
}

model Condition {
  id        String    @id @default(cuid())
  name      String    @unique
  status    Status    @default(NOT_ACTIVE)
  createdAt DateTime  @default(now()) @map("created_at")
  updatedAt DateTime  @updatedAt @map("updated_at")
  harvest   Harvest[]

  @@map("conditions")
}

model Block {
  id        String    @id @default(cuid())
  name      String    @unique
  status    Status    @default(NOT_ACTIVE)
  createdAt DateTime  @default(now()) @map("created_at")
  updatedAt DateTime  @updatedAt @map("updated_at")
  plant     Plant[]
  harvest   Harvest[]

  @@map("blocks")
}

model Presence {
  id                String             @id @default(cuid())
  date              DateTime           @unique
  createdAt         DateTime           @default(now()) @map("created_at")
  updatedAt         DateTime           @updatedAt @map("updated_at")
  employeesPresence EmployeePresence[]

  @@map("presences")
}

model Plant {
  id        String   @id @default(cuid())
  seed      Seed     @relation(fields: [seedId], references: [id], onDelete: Cascade)
  seedId    String   @map("seed_id")
  block     Block    @relation(fields: [blockId], references: [id], onDelete: Cascade)
  blockId   String   @map("block_id")
  plant     Int
  date      DateTime
  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@unique([date, seedId, blockId])
  @@map("plants")
}

model Harvest {
  id          String    @id @default(cuid())
  seed        Seed      @relation(fields: [seedId], references: [id], onDelete: Cascade)
  seedId      String    @map("seed_id")
  block       Block     @relation(fields: [blockId], references: [id], onDelete: Cascade)
  blockId     String    @map("block_id")
  condition   Condition @relation(fields: [conditionId], references: [id], onDelete: Cascade)
  conditionId String    @map("condition_id")
  harvest     Int
  date        DateTime
  createdAt   DateTime  @default(now()) @map("created_at")
  updatedAt   DateTime  @updatedAt @map("updated_at")

  @@unique([date, seedId, blockId, conditionId])
  @@map("harvests")
}

model EmployeePresence {
  id           String       @id @default(cuid())
  presence     Presence     @relation(fields: [presenceId], references: [id], onDelete: Cascade)
  presenceId   String
  presenceTime PresenceTime @map("presence_time")
  employee     Employee     @relation(fields: [employeeId], references: [id], onDelete: Cascade)
  employeeId   String
  presenceType PresenceType
  createdAt    DateTime     @default(now()) @map("created_at")
  updatedAt    DateTime     @updatedAt @map("updated_at")

  @@unique([presenceId, employeeId, presenceTime])
  @@map("employee_presences")
}
