import {
  CreateConditionUsecase,
  DeleteConditionUsecase,
  GetConditionByIdUsecase,
  GetConditionsPaginationUsecase,
  GetConditionsUsecase,
  UpdateConditionUsecase,
} from '@/Applications/usecase/Condition';
import { ClientError } from '@/common/exceptions';
import { ConditionPresentation } from '@/Domain/Condition';
import { parseIntOrNotNaN } from '@/utils';

export class ConditionHandler {
  constructor(container) {
    this.container = container;
  }

  async getConditionHandler(req, res) {
    const condition = await this.container
      .get(GetConditionByIdUsecase.name)
      .execute(req.params);
    const conditionPresentation =
      this.conditionToConditionPresentation(condition);

    res.status(200).json({ condition: conditionPresentation });
  }

  async getConditionsHandler(req, res) {
    const { name, status, skip, take } = req.query;
    const data = {
      name: name?.toLowerCase(),
      status: parseIntOrNotNaN(status),
      skip: parseIntOrNotNaN(skip),
      take: parseIntOrNotNaN(take),
    };
    const [conditions, conditionsPagination] = await Promise.all([
      this.container.get(GetConditionsUsecase.name).execute(data),
      this.container.get(GetConditionsPaginationUsecase.name).execute(data),
    ]);
    const conditionPresentations =
      this.conditionsToConditionPresentations(conditions);

    res
      .status(200)
      .json({ conditions: conditionPresentations, ...conditionsPagination });
  }

  async postConditionHandler(req, res) {
    const condition = await this.container
      .get(CreateConditionUsecase.name)
      .execute(req.body);
    const conditionPresentation =
      this.conditionToConditionPresentation(condition);

    res.status(201).json({ condition: conditionPresentation });
  }

  async putConditionHandler(req, res) {
    const condition = await this.container
      .get(UpdateConditionUsecase.name)
      .execute({ ...req.params, ...req.body });
    const conditionPresentation =
      this.conditionToConditionPresentation(condition);

    res.status(200).json({ condition: conditionPresentation });
  }

  async patchConditionHandler(req, res) {
    return this.putConditionHandler(req, res);
  }

  async deleteConditionHandler(req, res) {
    try {
      await this.container.get(DeleteConditionUsecase.name).execute(req.params);
      res.status(200).json('Successfully deleted the seed');
    } catch (error) {
      if (error instanceof ClientError) {
        res.status(200).json('Sucessfully deleted but no seed was deleted');
        return;
      }

      throw error;
    }
  }

  conditionToConditionPresentation({ id, name, status }) {
    return new ConditionPresentation({ id, name, status });
  }

  conditionsToConditionPresentations(conditions) {
    return conditions.map((condition) =>
      this.conditionToConditionPresentation(condition)
    );
  }
}
